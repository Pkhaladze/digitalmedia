
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8" />
    <title>Fullwidth Layout</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body class="gantry site com_gantry5 view-custom no-layout no-task dir-ltr itemid-236 outline-26 g-offcanvas-left g-default g-style-preset1">
<div id="g-offcanvas"  data-g-offcanvas-swipe="0" data-g-offcanvas-css3="1">
    <div class="g-grid">
        <div class="g-block size-100">
            <div class="g-content g-particle">
                <div id="g-mobilemenu-container" data-g-menu-breakpoint="60rem"></div>
            </div>
        </div>
    </div>
    <div class="g-grid">
        <div class="g-block size-100">
            <div class="g-content">
                <div class="platform-content">
                    <div class="moduletable hidden-phone hidden-tablet">
                        <h3 class="g-title"><span>Offcanvas Section</span></h3>
                        <div class="customhidden-phone hidden-tablet"  >
                            <p>You can publish whatever you want in the Offcanvas Section. It can be any module or particle.</p>
                            <p>By default, the available module positions are <strong>offcanvas-a</strong> and <strong>offcanvas-b </strong>but you can add as many module positions as you want from the Layout Manager.</p>
                            <p>You can also add the <strong>hidden-phone</strong> module class suffix to your modules so they do not appear in the Offcanvas Section when the site is loaded on a mobile device.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

  @yield('content')

<footer id="g-footer">
    <div class="g-container">
    <div class="g-grid">
        <div class="g-block size-33">
            <div class="g-content">
                    <div class="moduletable ">
                        <h3 class="g-title"><span>About Us</span></h3>
                        <div class="g-particle">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. </div>
                    </div>
                <div class="moduletable ">
                    <div class="g-particle">
                        <div class="g-contacts uk-margin-top" >
                            <div class="g-contacts-item" >
                                <span class="g-contacts-icon fa fa-home"></span>
                                <span class="g-contact-value">64184 Vincent Place</span>
                            </div>
                            <div class="g-contacts-item" >
                                <span class="g-contacts-icon fa fa-phone"></span>
                                <span class="g-contact-value">0044 889 555 432</span>
                            </div>
                            <div class="g-contacts-item" >
                                <span class="g-contacts-icon fa fa-envelope-o"></span>
                                <span class="g-contact-value">office@headlines.com</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="g-block size-34">
        <div class="g-content">
            <div class="moduletable ">
                <h3 class="g-title"><span>Recent News</span></h3>
                <div class="g-particle">
                    <div class="g-news-pro" >
                        <div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
                            <div class="uk-slider-container">
                                <div class="uk-slideset uk-grid">
                                    <div class="g-news-pro-page">
                                        <div class="uk-grid uk-grid-width-1-1">
                                            <div class="g-news-pro-item horizontal g-cat-business g-featured-article">
                                                <div class="g-news-pro-image image-link" style="background-image: url(images/business9.jpg); width: 120px; height: 75px;">
                                                    <a href="##"></a></div>
                                                <div class="g-info-container">
                                                    <h4 class="g-news-pro-title"><a href="##">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
                                                    <div class="g-article-details details-show">
                                                        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 15</span>
                                                        <span class="g-article-category">
                                                        <i class="fa fa-folder-open"></i>Business</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="uk-grid uk-grid-width-1-1">
                                            <div class="g-news-pro-item horizontal g-cat-lifestyle g-featured-article">
                                                <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle12.jpg); width: 120px; height: 75px;">
                                                    <a href="#"></a>  </div>
                                                <div class="g-info-container">
                                                    <h4 class="g-news-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed...</a></h4>
                                                    <div class="g-article-details details-show">
                                                        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 14</span>
                                                        <span class="g-article-category">
                                                        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="g-news-pro-page">
                                        <div class="uk-grid uk-grid-width-1-1">
                                        <div class="g-news-pro-item horizontal g-cat-lifestyle">
                                        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle11.jpg); width: 120px; height: 75px;">
                                        <a href="#"></a>
                                        </div>
                                        <div class="g-info-container">
                                        <h4 class="g-news-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi...</a></h4>
                                        <div class="g-article-details details-show">
                                        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 13</span>
                                        <span class="g-article-category">
                                        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="uk-grid uk-grid-width-1-1">
                                        <div class="g-news-pro-item horizontal g-cat-fashion">
                                        <div class="g-news-pro-image image-link" style="background-image: url(images/fashion9.jpg); width: 120px; height: 75px;">
                                        <a href="#"></a>
                                        </div>
                                        <div class="g-info-container">
                                        <h4 class="g-news-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh...</a></h4>
                                        <div class="g-article-details details-show">
                                        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
                                        <span class="g-article-category">
                                        <i class="fa fa-folder-open"></i>Fashion                                    </span>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="g-news-pro-page">
                                        <div class="uk-grid uk-grid-width-1-1">
                                            <div class="g-news-pro-item horizontal g-cat-lifestyle">
                                            <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle10.jpg); width: 120px; height: 75px;">
                                            <a href="#"></a>
                                            </div>
                                            <div class="g-info-container">
                                            <h4 class="g-news-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum...</a></h4>
                                            <div class="g-article-details details-show">
                                            <span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
                                            <span class="g-article-category">
                                            <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
                                            </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="uk-grid uk-grid-width-1-1">
                                            <div class="g-news-pro-item horizontal g-cat-business">
                                            <div class="g-news-pro-image image-link" style="background-image: url(images/business8.jpg); width: 120px; height: 75px;">
                                            <a href="#"></a>
                                            </div>
                                            <div class="g-info-container">
                                            <h4 class="g-news-pro-title"><a href="#">Maecenas facilisis dapibus lectus in metus odio</a></h4>
                                            <div class="g-article-details details-show">
                                            <span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
                                            <span class="g-article-category">
                                            <i class="fa fa-folder-open"></i>Business                                   </span>
                                            </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="g-news-pro-nav">
                                <div class="g-news-pro-arrows">
                                    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="g-block size-33">
    <div class="g-content">
    <div class="platform-content"><div class="moduletable ">
    <h3 class="g-title"><span>Newsletter</span></h3>
    <div class="acymailing_module" id="acymailing_module_formAcymailing73431">
    <div class="acymailing_fulldiv" id="acymailing_fulldiv_formAcymailing73431"  >
    <form id="formAcymailing73431" action="###" onsubmit="return submitacymailingform('optin','formAcymailing73431')" method="post" name="formAcymailing73431"  >
        <div class="acymailing_module_form" >
        <div class="acymailing_introtext">Stay tuned with our latest news!</div>    <table class="acymailing_form">
        <tr>
        <td class="acyfield_name acy_requiredField">
        <input id="user_name_formAcymailing73431"  onfocus="if(this.value == 'Name') this.value = '';" onblur="if(this.value=='') this.value='Name';" class="inputbox" type="text" name="user[name]" style="width:100%" value="Name" title="Name"/>
        </td> </tr><tr> <td class="acyfield_email acy_requiredField">
        <input id="user_email_formAcymailing73431"  onfocus="if(this.value == 'E-mail') this.value = '';" onblur="if(this.value=='') this.value='E-mail';" class="inputbox" type="text" name="user[email]" style="width:100%" value="E-mail" title="E-mail"/>
        </td> </tr><tr>
        <td  class="acysubbuttons">
        <input class="button subbutton btn btn-primary" type="submit" value="Subscribe" name="Submit" onclick="try{ return submitacymailingform('optin','formAcymailing73431'); }catch(err){alert('The form could not be submitted '+err);return false;}"/>
        </td>
        </tr>
        </table>
        <input type="hidden" name="ajax" value="0" />
        <input type="hidden" name="acy_source" value="module_211" />
        <input type="hidden" name="ctrl" value="sub"/>
        <input type="hidden" name="task" value="notask"/>
        <input type="hidden" name="option" value="com_acymailing"/>
        <input type="hidden" name="hiddenlists" value="1"/>
        <input type="hidden" name="acyformname" value="formAcymailing73431" />
        </div>
    </form>
    </div>
    </div>
    </div></div>
    </div>
    </div>
    </div>
    </div>
</footer>
    <section id="g-copyright">
        <div class="g-container">
            <div class="g-grid">
                <div class="g-block size-50">
                    <div class="g-content">
                        <div class="moduletable ">
                            <div class="g-particle">
                                <div class="g-branding branding">{{ date('Y') }} © Designed by <a href="##">me</a>.</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="g-block size-50">
                    <div class="g-content">
                        <div class="moduletable ">
                            <div class="g-particle">
                                <div class="g-social" >
                                    <a target="_blank" href="http://facebook.com" data-uk-tooltip title="Facebook"><span class="fa fa-facebook"></span></a>
                                    <a target="_blank" href="http://twitter.com" data-uk-tooltip title="Twitter"><span class="fa fa-twitter"></span></a>
                                    <a target="_blank" href="https://plus.google.com/" data-uk-tooltip title="Google+"><span class="fa fa-google-plus"></span></a>
                                    <a target="_blank" href="http://linkedin.com" data-uk-tooltip title="Linkedin"><span class="fa fa-linkedin"></span></a>
                                    <a target="_blank" href="http://dribbble.com" data-uk-tooltip title="Dribbble"><span class="fa fa-dribbble"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<section id="g-to-top">
    <div class="g-container">
        <div class="g-grid">
            <div class="g-block size-100">
                <div class="g-content g-particle">
                    <div class=" g-particle">
                        <div class="g-totop style1">
                            <a href="#" id="g-totop-button" rel="nofollow" data-uk-smooth-scroll>
                            <i class="fa fa-angle-up"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script type="application/javascript" src="js/library.js"></script>
</body>
</html>