<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/news', function () {
    return view('news');
});
Route::get('/travel', function () {
    return view('travel');
});
Route::get('/master', function () {
    return view('temporary.master');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
