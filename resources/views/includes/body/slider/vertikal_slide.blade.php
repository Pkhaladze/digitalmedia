        <div class="g-grid">
        <div class="g-block size-100">
        <div class="g-content">
        <div class="moduletable ">
        <h3 class="g-title"><span>Lifestyle</span></h3>
        <div class="g-particle">
        <div class="g-news-pro" >
        <div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
        <div class="uk-slider-container">
        <div class="uk-slideset uk-grid">
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle g-featured-article">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle12.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed sit amet</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 14</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>160</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle11.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi mauris orci</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 13</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>65</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle10.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eget turpis</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>56</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle9.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 11</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>54</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle8.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh mauris</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 10</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>37</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle7.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Lorem ipsum dolor sit amet, conse adipiscing</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 09</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>39</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle6.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/16-etiam-eu-sapien-at-purus-ultricies-tempor-nabh"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/16-etiam-eu-sapien-at-purus-ultricies-tempor-nabh">Etiam eu sapien at purus ultricies tempor nabh</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 08</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>30</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle5.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/15-class-aptent-taciti-socio-ad-litora-est-fringi"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/15-class-aptent-taciti-socio-ad-litora-est-fringi">Class aptent taciti socio ad litora est fringi</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 07</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>34</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle4.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/14-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/14-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum">Uisque ac gravida ligula nunc nisi risus ipsum</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 06</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>37</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle3.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/13-phasellus-eget-augue-est-fring-metus-felis"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/13-phasellus-eget-augue-est-fring-metus-felis">Phasellus eget augue est fring metus felis</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 05</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>53</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle2.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/12-proin-volutpat-tristique-diam-quis-euismod-sed"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/12-proin-volutpat-tristique-diam-quis-euismod-sed">Proin volutpat tristique diam quis euismod sed</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 04</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>42</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle1.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/11-aliquam-sodales-quam-in-era-neque-porttitor"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/11-aliquam-sodales-quam-in-era-neque-porttitor">Aliquam sodales quam in era neque porttitor</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 03</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>48</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-nav">
        <div class="g-news-pro-arrows">
        <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
        <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
        </div>
        <ul class="uk-slideset-nav uk-dotnav uk-flex-center">
            <li data-uk-slideset-item="0"><a href="#"></a></li>
            <li data-uk-slideset-item="1"><a href="#"></a></li>
            <li data-uk-slideset-item="2"><a href="#"></a></li>
            <li data-uk-slideset-item="3"><a href="#"></a></li>
            <li data-uk-slideset-item="4"><a href="#"></a></li>
            <li data-uk-slideset-item="5"><a href="#"></a></li>
            <li data-uk-slideset-item="6"><a href="#"></a></li>
            <li data-uk-slideset-item="7"><a href="#"></a></li>
            <li data-uk-slideset-item="8"><a href="#"></a></li>
            <li data-uk-slideset-item="9"><a href="#"></a></li>
            <li data-uk-slideset-item="10"><a href="#"></a></li>
            <li data-uk-slideset-item="11"><a href="#"></a></li>
        </ul>
        </div>
        </div>
        </div>
        </div>  </div>
        </div>
        </div>
        </div>