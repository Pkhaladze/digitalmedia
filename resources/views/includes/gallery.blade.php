
<section id="g-mainbottom" class="pull-up">
    <div class="g-container">
        <div class="g-grid">
            <div class="g-block size-100">
                <div class="g-content">
                    <div class="moduletable ">
                        <h3 class="g-title"><span>News Gallery</span></h3>
                        <div class="g-particle">
                            <div class="g-content-pro-slideset style2 uk-text-left gutter-enabled" >
                                <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 4, large: 4, duration: 200,  animation: 'fade'}">
                                <div class="uk-slider-container">
                                    <ul class="uk-slideset uk-grid">
                                        <li>
                                            <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-business g-featured-article">
                                                <div class="g-content-pro-image">
                                                    <a href="##">
                                                    <img src="images/business9.jpg" width="1000" height="500" alt="Vestibulum faucibus mollis tel massa id est dolor" /></a>
                                                </div>
                                                <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                                    <h4 class="g-content-pro-title"><a href="##">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
                                                    <div class="g-article-details details-show"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-lifestyle g-featured-article">
                                                <div class="g-content-pro-image">
                                                <a href="#">
                                                <img src="images/lifestyle12.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed sit amet" />
                                                </a>
                                                </div>
                                                <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed sit amet</a></h4>
                                                    <div class="g-article-details details-show"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                        <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-business">
                                        <div class="g-content-pro-image">
                                        <a href="#">
                                        <img src="images/business6.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor" />
                                        </a>
                                        </div>
                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h4 class="g-content-pro-title"><a href="#">Aliquam sodales quam in era neque porttitor</a></h4>
                                        <div class="g-article-details details-show">
                                        </div>
                                        </div>
                                        </div>
                                        </li>
                                        <li>
                                        <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-fashion">
                                        <div class="g-content-pro-image">
                                        <a href="/templates/headlines/index.php/fashion/32-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-mollis-felis">
                                        <img src="images/fashion1.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum mollis felis" />
                                        </a>
                                        </div>
                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/fashion/32-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-mollis-felis">Uisque ac gravida ligula nunc nisi risus ipsum mollis felis</a></h4>
                                        <div class="g-article-details details-show">
                                        </div>
                                        </div>
                                        </div>
                                        </li>
                                        <li>
                                        <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-business">
                                        <div class="g-content-pro-image">
                                        <a href="#">
                                        <img src="images/business8.jpg" width="1000" height="500" alt="Maecenas facilisis dapibus lectus in metus odio" />
                                        </a>
                                        </div>
                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h4 class="g-content-pro-title"><a href="#">Maecenas facilisis dapibus lectus in metus odio</a></h4>
                                        <div class="g-article-details details-show">
                                        </div>
                                        </div>
                                        </div>
                                        </li>
                                        <li>
                                        <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-technology g-featured-article">
                                        <div class="g-content-pro-image">
                                        <a href="#">
                                        <img src="images/technology8.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh auctor" />
                                        </a>
                                        </div>
                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h4 class="g-content-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh auctor</a></h4>
                                        <div class="g-article-details details-show">
                                        </div>
                                        </div>
                                        </div>
                                        </li>
                                        <li>
                                        <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-business">
                                        <div class="g-content-pro-image">
                                        <a href="#">
                                        <img src="images/business7.jpg" width="1000" height="500" alt="Donec pulvinar nisi vitae odio suscipit sit amet lectu" />
                                        </a>
                                        </div>
                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h4 class="g-content-pro-title"><a href="#">Donec pulvinar nisi vitae odio suscipit sit amet lectu</a></h4>
                                        <div class="g-article-details details-show">
                                        </div>
                                        </div>
                                        </div>
                                        </li>
                                        <li>
                                        <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-technology">
                                        <div class="g-content-pro-image">
                                        <a href="#">
                                        <img src="images/technology9.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis sit amet" />
                                        </a>
                                        </div>
                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h4 class="g-content-pro-title"><a href="#">Phasellus eget augue est fring metus felis sit amet</a></h4>
                                        <div class="g-article-details details-show">
                                        </div>
                                        </div>
                                        </div>
                                        </li>
                                        <li>
                                        <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-fashion g-featured-article">
                                        <div class="g-content-pro-image">
                                        <a href="#">
                                        <img src="images/fashion7.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
                                        </a>
                                        </div>
                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h4 class="g-content-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
                                        <div class="g-article-details details-show">
                                        </div>
                                        </div>
                                        </div>
                                        </li>
                                        <li>
                                        <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-business">
                                        <div class="g-content-pro-image">
                                        <a href="/templates/headlines/index.php/business/2-etiam-eu-sapien-at-purus-ultricies-tempor-nabh">
                                        <img src="images/business1.jpg" width="1000" height="499" alt=" Etiam eu sapien at purus ultricies tempor nabh" />
                                        </a>
                                        </div>
                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/business/2-etiam-eu-sapien-at-purus-ultricies-tempor-nabh"> Etiam eu sapien at purus ultricies tempor nabh</a></h4>
                                        <div class="g-article-details details-show">
                                        </div>
                                        </div>
                                        </div>
                                        </li>
                                        <li>
                                        <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-lifestyle">
                                        <div class="g-content-pro-image">
                                        <a href="#">
                                        <img src="images/lifestyle11.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi mauris orci" />
                                        </a>
                                        </div>
                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                        <h4 class="g-content-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi mauris orci</a></h4>
                                        <div class="g-article-details details-show">
                                        </div>
                                        </div>
                                        </div>
                                        </li>
                                        <li>
                                            <div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-fashion">
                                                <div class="g-content-pro-image">
                                                    <a href="#"><img src="images/fashion8.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" /></a>
                                                </div>
                                                <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
                                                    <div class="g-article-details details-show"></div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>