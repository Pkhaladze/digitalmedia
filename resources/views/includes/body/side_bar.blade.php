


    <div class="g-block size-33">
        <aside id="g-aside">
            <div class="g-grid">
            <div class="g-block size-100">
            <div class="g-content">
            <div class="moduletable box2">
            <h3 class="g-title"><span>Most Popular</span></h3>
            <div class="g-particle">
            <div class="g-content-pro style1 uk-text-left gutter-enabled" >
            <div class="g-grid">
            <div class="g-block">
            <div class="g-content">
            <div class="g-content-pro-item g-cat-business g-featured-article">
            <div class="g-content-pro-image">
            <a href="##">
            <img src="images/business9.jpg" width="1000" height="500" alt="Vestibulum faucibus mollis tel massa id est dolor" />
            </a>
            </div>
            <div class="g-info-container">
            <h4 class="g-content-pro-title"><a href="##">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-date"><i class="fa fa-clock-o"></i>September 15</span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>326</span>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>  <div class="g-grid">
            <div class="g-block">
            <div class="g-content">
            <div class="g-content-pro-item g-cat-lifestyle g-featured-article">
            <div class="g-content-pro-image">
            <a href="#">
            <img src="images/lifestyle12.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed sit amet" />
            </a>
            </div>
            <div class="g-info-container">
            <h4 class="g-content-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed sit amet</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-date"><i class="fa fa-clock-o"></i>September 14</span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>160</span>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>  </div><div class="moduletable box4">
            <h3 class="g-title"><span>Editors Choice</span></h3>
            <div class="g-particle">
            <div class="g-news-pro" >
            <div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
            <div class="uk-slider-container">
            <div class="uk-slideset uk-grid">
            <div class="g-news-pro-page">
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item vertical g-cat-lifestyle">
            <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/lifestyle1.jpg);  height: 150px;">
            <a href="/templates/headlines/index.php/lifestyle/11-aliquam-sodales-quam-in-era-neque-porttitor"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/11-aliquam-sodales-quam-in-era-neque-porttitor">Aliquam sodales quam in era neque...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-date"><i class="fa fa-clock-o"></i>September 03</span>
            </div>
            <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
            </div>
            </div>
            </div>
            </div>
            <div class="g-news-pro-page">
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item vertical g-cat-lifestyle">
            <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/lifestyle2.jpg);  height: 150px;">
            <a href="/templates/headlines/index.php/lifestyle/12-proin-volutpat-tristique-diam-quis-euismod-sed"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/12-proin-volutpat-tristique-diam-quis-euismod-sed">Proin volutpat tristique diam quis...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-date"><i class="fa fa-clock-o"></i>September 04</span>
            </div>
            <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
            </div>
            </div>
            </div>
            </div>
            <div class="g-news-pro-page">
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item vertical g-cat-technology">
            <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology1.jpg);  height: 150px;">
            <a href="/templates/headlines/index.php/technology/23-aliquam-sodales-quam-in-era-neque-porttitor-lorem-ipsum"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/technology/23-aliquam-sodales-quam-in-era-neque-porttitor-lorem-ipsum">Aliquam sodales quam in era neque...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-date"><i class="fa fa-clock-o"></i>September 04</span>
            </div>
            <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            <div class="g-news-pro-nav">
            <div class="g-news-pro-arrows">
            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
            </div>
            <ul class="uk-slideset-nav uk-dotnav uk-flex-center">
                <li data-uk-slideset-item="0"><a href="#"></a></li>
                <li data-uk-slideset-item="1"><a href="#"></a></li>
                <li data-uk-slideset-item="2"><a href="#"></a></li>
                <li data-uk-slideset-item="3"><a href="#"></a></li>
                <li data-uk-slideset-item="4"><a href="#"></a></li>
                <li data-uk-slideset-item="5"><a href="#"></a></li>
                <li data-uk-slideset-item="6"><a href="#"></a></li>
                <li data-uk-slideset-item="7"><a href="#"></a></li>
                <li data-uk-slideset-item="8"><a href="#"></a></li>
                <li data-uk-slideset-item="9"><a href="#"></a></li>
                <li data-uk-slideset-item="10"><a href="#"></a></li>
                <li data-uk-slideset-item="11"><a href="#"></a></li>
                <li data-uk-slideset-item="12"><a href="#"></a></li>
                <li data-uk-slideset-item="13"><a href="#"></a></li>
                <li data-uk-slideset-item="14"><a href="#"></a></li>
                <li data-uk-slideset-item="15"><a href="#"></a></li>
                <li data-uk-slideset-item="16"><a href="#"></a></li>
                <li data-uk-slideset-item="17"><a href="#"></a></li>
                <li data-uk-slideset-item="18"><a href="#"></a></li>
                <li data-uk-slideset-item="19"><a href="#"></a></li>
                <li data-uk-slideset-item="20"><a href="#"></a></li>
                <li data-uk-slideset-item="21"><a href="#"></a></li>
                <li data-uk-slideset-item="22"><a href="#"></a></li>
                <li data-uk-slideset-item="23"><a href="#"></a></li>
                <li data-uk-slideset-item="24"><a href="#"></a></li>
                <li data-uk-slideset-item="25"><a href="#"></a></li>
                <li data-uk-slideset-item="26"><a href="#"></a></li>
                <li data-uk-slideset-item="27"><a href="#"></a></li>
                <li data-uk-slideset-item="28"><a href="#"></a></li>
                <li data-uk-slideset-item="29"><a href="#"></a></li>
                <li data-uk-slideset-item="30"><a href="#"></a></li>
                <li data-uk-slideset-item="31"><a href="#"></a></li>
                <li data-uk-slideset-item="32"><a href="#"></a></li>
                <li data-uk-slideset-item="33"><a href="#"></a></li>
                <li data-uk-slideset-item="34"><a href="#"></a></li>
                <li data-uk-slideset-item="35"><a href="#"></a></li>
                <li data-uk-slideset-item="36"><a href="#"></a></li>
                <li data-uk-slideset-item="37"><a href="#"></a></li>
                <li data-uk-slideset-item="38"><a href="#"></a></li>
                <li data-uk-slideset-item="39"><a href="#"></a></li>
                <li data-uk-slideset-item="40"><a href="#"></a></li>
                <li data-uk-slideset-item="41"><a href="#"></a></li>
                <li data-uk-slideset-item="42"><a href="#"></a></li>
                <li data-uk-slideset-item="43"><a href="#"></a></li>
            </ul>
            </div>
            </div>
            </div>
            </div>  </div><div class="moduletable box2">
            <h3 class="g-title"><span>Recent News</span></h3>
            <div class="g-particle">
            <div class="g-news-pro" >
            <div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
            <div class="uk-slider-container">
            <div class="uk-slideset uk-grid">
            <div class="g-news-pro-page">
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item horizontal g-cat-business g-featured-article">
            <div class="g-news-pro-image image-link" style="background-image: url(images/business9.jpg); width: 120px; height: 75px;">
            <a href="##"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="##">Vestibulum faucibus mollis tel massa...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-category">
            <i class="fa fa-folder-open"></i>Business                                   </span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>326</span>
            </div>
            </div>
            </div>
            </div>
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item horizontal g-cat-lifestyle g-featured-article">
            <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle12.jpg); width: 120px; height: 75px;">
            <a href="#"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="#">Proin volutpat tristique diam quis...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-category">
            <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>160</span>
            </div>
            </div>
            </div>
            </div>
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item horizontal g-cat-lifestyle">
            <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle11.jpg); width: 120px; height: 75px;">
            <a href="#"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="#">Class aptent taciti socio ad litora...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-category">
            <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>65</span>
            </div>
            </div>
            </div>
            </div>
            </div>
            <div class="g-news-pro-page">
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item horizontal g-cat-fashion">
            <div class="g-news-pro-image image-link" style="background-image: url(images/fashion9.jpg); width: 120px; height: 75px;">
            <a href="#"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="#">Etiam eu sapien at purus ultricies...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-category">
            <i class="fa fa-folder-open"></i>Fashion                                    </span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>53</span>
            </div>
            </div>
            </div>
            </div>
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item horizontal g-cat-lifestyle">
            <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle10.jpg); width: 120px; height: 75px;">
            <a href="#"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-category">
            <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>56</span>
            </div>
            </div>
            </div>
            </div>
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item horizontal g-cat-business">
            <div class="g-news-pro-image image-link" style="background-image: url(images/business8.jpg); width: 120px; height: 75px;">
            <a href="#"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="#">Maecenas facilisis dapibus lectus in...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-category">
            <i class="fa fa-folder-open"></i>Business                                   </span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>83</span>
            </div>
            </div>
            </div>
            </div>
            </div>
            <div class="g-news-pro-page">
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item horizontal g-cat-fashion">
            <div class="g-news-pro-image image-link" style="background-image: url(images/fashion8.jpg); width: 120px; height: 75px;">
            <a href="#"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="#">Phasellus eget augue est fring metus...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-category">
            <i class="fa fa-folder-open"></i>Fashion                                    </span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>64</span>
            </div>
            </div>
            </div>
            </div>
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item horizontal g-cat-technology">
            <div class="g-news-pro-image image-link" style="background-image: url(images/technology9.jpg); width: 120px; height: 75px;">
            <a href="#"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="#">Phasellus eget augue est fring metus...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-category">
            <i class="fa fa-folder-open"></i>Technology                                 </span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>76</span>
            </div>
            </div>
            </div>
            </div>
            <div class="uk-grid uk-grid-width-1-1">
            <div class="g-news-pro-item horizontal g-cat-lifestyle">
            <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle9.jpg); width: 120px; height: 75px;">
            <a href="#"></a>
            </div>
            <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="#">Phasellus eget augue est fring metus...</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-category">
            <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
            <span class="g-article-hits">
            <i class="fa fa-eye"></i>54</span>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            <div class="g-news-pro-nav">
            <div class="g-news-pro-arrows">
            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
            </div>
            </div>
            </div>
            </div>
            </div>  </div>
            </div>
            </div>
            </div>
        </aside>
    </div>
