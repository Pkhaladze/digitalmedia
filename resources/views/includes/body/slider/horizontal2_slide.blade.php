
        <div class="g-grid">
        <div class="g-block size-100">
        <div class="g-content">
        <div class="moduletable ">
        <h3 class="g-title"><span>Technology</span></h3>
        <div class="g-particle">
        <div class="g-news-pro" >
        <div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
        <div class="uk-slider-container">
        <div class="uk-slideset uk-grid">
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-3">
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology9.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Phasellus eget augue est fring metus felis sit...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 11</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology g-featured-article">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology8.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 10</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology7.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/technology/29-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/technology/29-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus">Aliquam sodales quam in era neque porttitor quis...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 09</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-3">
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology6.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 08</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology5.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 07</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology4.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 07</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-3">
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology3.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/technology/25-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor-2"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/technology/25-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor-2">Phasellus eget augue est fring metus felis ipsum...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 06</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology2.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/technology/24-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci-2"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/technology/24-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci-2">Etiam eu sapien at purus ultricies temor nabh...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 05</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology1.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/technology/23-aliquam-sodales-quam-in-era-neque-porttitor-lorem-ipsum"></a>
        </div>
        <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/technology/23-aliquam-sodales-quam-in-era-neque-porttitor-lorem-ipsum">Aliquam sodales quam in era neque porttitor lorem</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-date"><i class="fa fa-clock-o"></i>September 04</span>
            </div>
            <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-nav">
        <div class="g-news-pro-arrows">
            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
        </div>
        <ul class="uk-slideset-nav uk-dotnav uk-flex-center">
            <li data-uk-slideset-item="0"><a href="#"></a></li>
            <li data-uk-slideset-item="1"><a href="#"></a></li>
            <li data-uk-slideset-item="2"><a href="#"></a></li>
            <li data-uk-slideset-item="3"><a href="#"></a></li>
            <li data-uk-slideset-item="4"><a href="#"></a></li>
            <li data-uk-slideset-item="5"><a href="#"></a></li>
            <li data-uk-slideset-item="6"><a href="#"></a></li>
            <li data-uk-slideset-item="7"><a href="#"></a></li>
            <li data-uk-slideset-item="8"><a href="#"></a></li>
        </ul>
        </div>
        </div>
        </div>
        </div>  </div>
        </div>
        </div>
        </div>