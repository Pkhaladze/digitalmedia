<section id="g-navigation" data-uk-sticky="">
    <div class="g-container">
        <div class="g-grid">
        <div class="g-block size-75">
            <div class="g-content g-particle">
                <nav class="g-main-nav" role="navigation" data-g-mobile-target data-g-hover-expand="true">
                    <ul class="g-toplevel">
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-101 g-parent active g-standard g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="#">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">Home</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade g-dropdown-right">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                        <div class="g-block size-100">
                                            <ul class="g-sublevel">
                                                <li class="g-level-1 g-go-back">
                                                    <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                                </li>
                                                <li class="g-menu-item g-menu-item-type-component g-menu-item-125  ">
                                                    <a class="g-menu-item-container" href="#">
                                                        <span class="g-menu-item-content">
                                                            <span class="g-menu-item-title">Home 1 (Default)</span>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="g-menu-item g-menu-item-type-component g-menu-item-126  ">
                                                    <a class="g-menu-item-container" href="#">
                                                        <span class="g-menu-item-content">
                                                            <span class="g-menu-item-title">Home 2</span>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="g-menu-item g-menu-item-type-component g-menu-item-236 active  ">
                                                    <a class="g-menu-item-container" href="###">
                                                        <span class="g-menu-item-content">
                                                            <span class="g-menu-item-title">Fullwidth Layout</span>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-229 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="/templates/headlines/index.php/business">
                                <span class="g-menu-item-content"><span class="g-menu-item-title">Business</span></span>
                                <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                        <div class="g-block size-100">
                                            <ul class="g-sublevel">
                                                <li class="g-level-1 g-go-back"><a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a></li>
                                                <li class="g-menu-item g-menu-item-type-particle g-menu-item-business---particle-0vCDc  ">
                                                    <div class="g-menu-item-container" data-g-menuparent="">
                                                        <div class="menu-item-particle">
                                                            <div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
                                                                <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
                                                                    <div class="uk-slider-container">
                                                                        <ul class="uk-slideset uk-grid">
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business g-featured-article">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="##">
                                                                                        <img src="images/business9.jpg" width="1000" height="500" alt="Vestibulum faucibus mollis tel massa id est dolor" />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="##">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
                                                                                            <div class="g-article-details details-">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#">
                                                                                            <img src="images/business8.jpg" width="1000" height="500" alt="Maecenas facilisis dapibus lectus in metus odio" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Maecenas facilisis dapibus lectus in metus odio</a></h4>
                                                                                        <div class="g-article-details details-"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business">
                                                                                    <div class="g-content-pro-image">
                                                                                    <a href="#">
                                                                                        <img src="images/business7.jpg" width="1000" height="500" alt="Donec pulvinar nisi vitae odio suscipit sit amet lectu" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Donec pulvinar nisi vitae odio suscip</a></h4>
                                                                                        <div class="g-article-details details-"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#">
                                                                                            <img src="images/business6.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Aliquam sodales quam in era neque porttitor</a></h4>
                                                                                        <div class="g-article-details details-"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#">
                                                                                        <img src="images/business5.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed</a></h4>
                                                                                        <div class="g-article-details details-">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#">
                                                                                        <img src="images/business4.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi</a></h4>
                                                                                        <div class="g-article-details details-">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                                                    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-230 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="#">
                                <span class="g-menu-item-content">
                                    <span class="g-menu-item-title">Lifestyle</span>
                                </span>
                                <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                        <div class="g-block size-100">
                                            <ul class="g-sublevel">
                                                <li class="g-level-1 g-go-back">
                                                    <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                                </li>
                                                <li class="g-menu-item g-menu-item-type-particle g-menu-item-lifestyle---particle-UsJxk  ">
                                                    <div class="g-menu-item-container" data-g-menuparent="">
                                                        <div class="menu-item-particle">
                                                            <div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
                                                                <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
                                                                    <div class="uk-slider-container">
                                                                        <ul class="uk-slideset uk-grid">
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle g-featured-article">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"><img src="images/lifestyle12.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed sit amet" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed sit amet</a></h4>
                                                                                        <div class="g-article-details details-"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"><img src="images/lifestyle11.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi mauris orci" />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi mauris orci</a></h4>
                                                                                        <div class="g-article-details details-"> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"> <img src="images/lifestyle10.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eget turpis" /> </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eget turpis</a></h4>
                                                                                        <div class="g-article-details details-"> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"> <img src="images/lifestyle9.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" /> </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
                                                                                        <div class="g-article-details details-"> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"> <img src="images/lifestyle8.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh mauris" /> </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh mauris</a></h4>
                                                                                        <div class="g-article-details details-"> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"> <img src="images/lifestyle7.jpg" width="1000" height="500" alt="Lorem ipsum dolor sit amet, conse adipiscing" /> </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Lorem ipsum dolor sit amet, conse adipiscing</a></h4>
                                                                                        <div class="g-article-details details-"> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                                                    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-231 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="#">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">Technology</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                    <div class="g-block size-100">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-particle g-menu-item-technology---particle-6i1qf  ">
                                            <div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
                                            <div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
                                            <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
                                            <div class="uk-slider-container">
                                            <ul class="uk-slideset uk-grid">
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology">
                                                        <div class="g-content-pro-image">
                                                        <a href="#">
                                                        <img src="images/technology9.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis sit amet" />
                                                        </a>
                                                        </div>
                                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                        <h4 class="g-content-pro-title"><a href="#">Phasellus eget augue est fring metus felis sit amet</a></h4>
                                                        <div class="g-article-details details-">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology g-featured-article">
                                                        <div class="g-content-pro-image">
                                                        <a href="#">
                                                        <img src="images/technology8.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh auctor" />
                                                        </a>
                                                        </div>
                                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                        <h4 class="g-content-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh auctor</a></h4>
                                                        <div class="g-article-details details-">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/technology/29-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus">
                                                    <img src="images/technology7.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor quis metus" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/technology/29-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus">Aliquam sodales quam in era neque porttitor quis metus</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology">
                                                    <div class="g-content-pro-image">
                                                    <a href="#">
                                                    <img src="images/technology6.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed posuere" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed posuere</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology">
                                                    <div class="g-content-pro-image">
                                                    <a href="#">
                                                    <img src="images/technology5.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi non mi enim" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi non mi enim</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology">
                                                        <div class="g-content-pro-image">
                                                        <a href="#">
                                                        <img src="images/technology4.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
                                                        </a>
                                                        </div>
                                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                        <h4 class="g-content-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
                                                        <div class="g-article-details details-">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            </div>
                                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-232 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="/templates/headlines/index.php/fashion">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">Fashion</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                    <div class="g-block size-100">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-particle g-menu-item-fashion---particle-1Ex12  ">
                                            <div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
                                            <div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
                                            <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
                                            <div class="uk-slider-container">
                                            <ul class="uk-slideset uk-grid">
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion">
                                                    <div class="g-content-pro-image">
                                                    <a href="#">
                                                    <img src="images/fashion9.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh mauris" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh mauris</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion">
                                                    <div class="g-content-pro-image">
                                                    <a href="#">
                                                    <img src="images/fashion8.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion g-featured-article">
                                                        <div class="g-content-pro-image">
                                                            <a href="#">
                                                            <img src="images/fashion7.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
                                                            </a>
                                                        </div>
                                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                        <h4 class="g-content-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
                                                        <div class="g-article-details details-">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion">
                                                        <div class="g-content-pro-image">
                                                            <a href="#">
                                                            <img src="images/fashion6.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi non mi enim" />
                                                            </a>
                                                        </div>
                                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                        <h4 class="g-content-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi non mi enim</a></h4>
                                                        <div class="g-article-details details-">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/fashion/37-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">
                                                    <img src="images/fashion5.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed posuere" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/fashion/37-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">Proin volutpat tristique diam quis euismod sed posuere</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion">
                                                    <div class="g-content-pro-image">
                                                    <a href="#">
                                                    <img src="images/fashion4.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor quis metus" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Aliquam sodales quam in era neque porttitor quis metus</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            </div>
                                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-233 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="/templates/headlines/index.php/travel">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">Travel</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                    <div class="g-block size-100">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-particle g-menu-item-travel---particle-QRP9Y  ">
                                            <div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
                                            <div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
                                            <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
                                            <div class="uk-slider-container">
                                            <ul class="uk-slideset uk-grid">
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-travel">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/travel/46-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">
                                                    <img src="images/travel9.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed posuere" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/46-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">Proin volutpat tristique diam quis euismod sed posuere</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-travel">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/travel/45-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis">
                                                    <img src="images/travel8.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi non mi enim" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/45-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis">Class aptent taciti socio ad litora est fringi non mi enim</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-travel">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/travel/44-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan">
                                                    <img src="images/travel7.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/44-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-travel">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/travel/43-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor">
                                                    <img src="images/travel6.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/43-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-travel g-featured-article">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">
                                                    <img src="images/travel5.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultri tem nabh mauris orci" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">Etiam eu sapien at purus ultri tem nabh mauris orci</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            </div>
                                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-url g-menu-item-150 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="#">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">More</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                    <div class="g-block size-25">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-151  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/contact-us">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Contact Us</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-152  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/about-us">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">About Us</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-153  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/roksprocket-mosaic-default">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">RokSprocket Mosaic (Default)</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-154  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/roksprocket-mosaic-gallery">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">RokSprocket Mosaic (Gallery)</span>
                                            </span>
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                    <div class="g-block size-25">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-168  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/search-results">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Search Results</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-176  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/member-login">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Member Login</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-178  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/registration-form">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Registration Form</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-181  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/pagessss">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">404 Page</span>
                                            </span>
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                    <div class="g-block size-25">
                                        <ul class="g-sublevel">
                                            <li class="g-level-1 g-go-back">
                                                <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                            </li>
                                            <li class="g-menu-item g-menu-item-type-component g-menu-item-192  ">
                                                <a class="g-menu-item-container" href="/templates/headlines/index.php/more/module-positions">
                                                <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">Layout Manager</span>
                                                </span>
                                                </a>
                                            </li>
                                            <li class="g-menu-item g-menu-item-type-component g-menu-item-193  ">
                                                <a class="g-menu-item-container" href="/templates/headlines/index.php/more/menu-editor">
                                                <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">Menu Editor</span>
                                                </span>
                                                </a>
                                            </li>
                                            <li class="g-menu-item g-menu-item-type-component g-menu-item-194  ">
                                                <a class="g-menu-item-container" href="/templates/headlines/index.php/more/particles">
                                                <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">Particles</span>
                                                </span>
                                                </a>
                                            </li>
                                                <li class="g-menu-item g-menu-item-type-component g-menu-item-197  ">
                                                <a class="g-menu-item-container" href="/templates/headlines/index.php/more/uikit-framework">
                                                <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">UIkit Framework</span>
                                                </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="g-block size-25">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-198  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/scrollreveal-js">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">ScrollReveal.js</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-191  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/module-variations">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Module Variations</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-196  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/typography">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Typography</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-199  ">
                                            <a class="g-menu-item-container" href="###" target="_blank">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Documentation</span>
                                            </span>
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-url g-menu-item-184 g-parent g-standard g-menu-item-link-parent presets-demo">
                            <a class="g-menu-item-container" href="#">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">Presets</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade g-dropdown-right">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                    <div class="g-block size-100">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-185  preset1">
                                            <a class="g-menu-item-container" href="/templates/headlines/?presets=preset1">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Preset 1 (Default)</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-186  preset2">
                                        <a class="g-menu-item-container" href="/templates/headlines/?presets=preset2">
                                        <span class="g-menu-item-content">
                                        <span class="g-menu-item-title">Preset 2</span>
                                        </span>
                                        </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-187  preset3">
                                        <a class="g-menu-item-container" href="/templates/headlines/?presets=preset3">
                                        <span class="g-menu-item-content">
                                        <span class="g-menu-item-title">Preset 3</span>
                                        </span>
                                        </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-188  preset4">
                                        <a class="g-menu-item-container" href="/templates/headlines/?presets=preset4">
                                        <span class="g-menu-item-content">
                                        <span class="g-menu-item-title">Preset 4</span>
                                        </span>
                                        </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-189  preset5">
                                        <a class="g-menu-item-container" href="/templates/headlines/?presets=preset5">
                                        <span class="g-menu-item-content">
                                        <span class="g-menu-item-title">Preset 5</span>
                                        </span>
                                        </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-190  preset6">
                                        <a class="g-menu-item-container" href="/templates/headlines/?presets=preset6">
                                        <span class="g-menu-item-content">
                                        <span class="g-menu-item-title">Preset 6</span>
                                        </span>
                                        </a>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="g-block size-25">
        <div class="g-content">
        <div class="moduletable ">
        <div class="g-particle">
        <div class="g-search-login" >
        <div class="g-buttons">
            <div class="g-search-button">
            <a id="header-search-toggle" href="#" data-uk-tooltip title="Search">
            <i class="fa fa-search"></i>
            </a>
            </div>
            <div class="g-login-button">
            <a href="#modal-login" data-uk-modal="{center:true}" data-uk-tooltip title="Login">
            <i class="fa fa-user"></i>
            </a>
            </div>
            <div class="g-offcanvas-button">
            <a class="offcanvas-toggle-particle" data-offcanvas-toggle="" data-uk-tooltip title="Offcanvas">
            <i class="fa fa-bars"></i>
            </a>
            </div>
        </div>
        <div id="header-search">
            <div class="g-block">
            <div class="g-content">
            <a class="uk-close"></a>
            <div class="moduletable">
            <div class="search mod_search93">
            <form action="/templates/headlines/index.php" method="post" class="form-inline">
            <label for="mod-search-searchword" class="element-invisible">Search ...</label> <input autocomplete="off" name="searchword" id="mod-search-searchword" maxlength="200"  class="inputbox search-query" type="search" size="20" placeholder="Search ..." />   <input type="hidden" name="task" value="search" />
            <input type="hidden" name="option" value="com_search" />
            <input type="hidden" name="Itemid" value="101" />
            </form>
            </div>
            </div>
            </div>
            </div>
        </div>
        <div id="modal-login" class="uk-modal">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-dialog">
            <div class="moduletabletitle-border">
                <h3>Login To Your Account</h3>
                <form action="##" method="post" id="login-form" class="form-inline">
                    <div class="userdata">
                    <div id="form-login-username" class="control-group">
                    <div class="controls">
                    <label for="modlgn-username" class="element-invisible">Username</label>
                    <input id="modlgn-username" type="text" name="username" tabindex="0" size="18" placeholder="Username" />
                    </div>
                    </div>
                    <div id="form-login-password" class="control-group">
                        <div class="controls">
                        <label for="modlgn-passwd" class="element-invisible">Password</label>
                        <input id="modlgn-passwd" type="password" name="password" tabindex="0" size="18" placeholder="Password" />
                        </div>
                    </div>
                    <div id="form-login-remember" class="control-group checkbox">
                    <label for="modlgn-remember" class="control-label">Remember Me</label> <input id="modlgn-remember" type="checkbox" name="remember" class="inputbox" value="yes"/>
                    </div>
                    <div id="form-login-submit" class="control-group">
                    <div class="controls">
                    <button type="submit" tabindex="0" name="Submit" class="btn btn-primary">Log in</button>
                    </div>
                    </div>
                    <ul class="unstyled">
                        <li>
                        <a href="/templates/headlines/index.php/more/registration-form">
                        <i class="fa fa-question-circle"></i>Create an account</a>
                        </li>
                        <li>
                        <a href="/templates/headlines/index.php/more/registration-form?view=remind">
                        <i class="fa fa-question-circle"></i>Forgot your username?</a>
                        </li>
                        <li>
                        <a href="/templates/headlines/index.php/more/registration-form?view=reset">
                        <i class="fa fa-question-circle"></i>Forgot your password?</a>
                        </li>
                    </ul>
                    <input type="hidden" name="option" value="com_users" />
                    <input type="hidden" name="task" value="user.login" />
                    <input type="hidden" name="return" value="aHR0cDovL2RlbW8uaW5zcGlyZXRoZW1lLmNvbS90ZW1wbGF0ZXMvaGVhZGxpbmVzLw==" />
                    <input type="hidden" name="0d14b02b3838edaccae7d7d9c3c6cf86" value="1" />   </div>
                </form>
            </div>
        </div>
        </div>
        </div>
        </div>  </div>
        </div>
        </div>
        </div>
    </div>
</section>