
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="utf-8" />
<meta name="generator" content="Joomla! - Open Source Content Management" />
<title>Travel</title>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body class="gantry site com_content view-category layout-blog no-task dir-ltr itemid-233 outline-25 g-offcanvas-left g-default g-style-preset1">
<div id="g-offcanvas"  data-g-offcanvas-swipe="0" data-g-offcanvas-css3="1">
<div          
         class="g-grid">
<div         
         class="g-block size-100">
<div class="g-content g-particle">
<div id="g-mobilemenu-container" data-g-menu-breakpoint="60rem"></div>
</div>
</div>
</div>
<div          
         class="g-grid">
<div         
         class="g-block size-100">
<div class="g-content">
<div class="platform-content"><div class="moduletable hidden-phone hidden-tablet">
<h3 class="g-title"><span>Offcanvas Section</span></h3>
<div class="customhidden-phone hidden-tablet"  >
<p>You can publish whatever you want in the Offcanvas Section. It can be any module or particle.</p>
<p>By default, the available module positions are <strong>offcanvas-a</strong> and <strong>offcanvas-b </strong>but you can add as many module positions as you want from the Layout Manager.</p>
<p>You can also add the <strong>hidden-phone</strong> module class suffix to your modules so they do not appear in the Offcanvas Section when the site is loaded on a mobile device.</p></div>
</div></div>
</div>
</div>
</div>
</div>
<div id="g-page-surround">
<div class="g-offcanvas-hide g-offcanvas-toggle" data-offcanvas-toggle><i class="fa fa-fw fa-bars"></i></div>
<section id="g-container-site" class="g-wrapper">
<div class="g-container"> <section id="g-top">
<div          
         class="g-grid">
<div         
         class="g-block size-60">
<div class="g-content">
<div class="moduletable ">
<div class="g-particle">
<nav class="g-main-nav" role="navigation" data-g-hover-expand="true">
<ul class="g-toplevel">
<li class="g-menu-item g-menu-item-type-url g-menu-item-128 g-standard  ">
<a class="g-menu-item-container" href="#">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Advertise</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-129 g-standard  ">
<a class="g-menu-item-container" href="#">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Contact Us</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-130 g-standard  ">
<a class="g-menu-item-container" href="#">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Write For Us</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-131 g-parent g-standard g-menu-item-link-parent ">
<a class="g-menu-item-container" href="#">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Dropdown</span>
</span>
<span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
<ul class="g-dropdown g-inactive g-fade g-dropdown-right">
<li class="g-dropdown-column">
<div class="g-grid">
<div class="g-block size-100">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-132  ">
<a class="g-menu-item-container" href="#">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Subitem 1</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-133  ">
<a class="g-menu-item-container" href="#">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Subitem 2</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-134  ">
<a class="g-menu-item-container" href="#">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Subitem 3</span>
</span>
</a>
</li>
</ul>
</div>
</div>
</li>
</ul>
</li>
</ul>
</nav>
</div>  </div>
</div>
</div>
<div         
         class="g-block hidden-phone size-40">
<div class="g-content">
<div class="moduletable ">
<div class="g-particle">
<div class="g-social" >
<a target="_blank" href="http://facebook.com" data-uk-tooltip="{pos:'bottom'}" title="Facebook"   >
<span class="fa fa-facebook fa-fw"></span>
</a>
<a target="_blank" href="http://twitter.com" data-uk-tooltip="{pos:'bottom'}" title="Twitter"   >
<span class="fa fa-twitter fa-fw"></span>
</a>
<a target="_blank" href="https://plus.google.com/" data-uk-tooltip="{pos:'bottom'}" title="Google+"   >
<span class="fa fa-google-plus fa-fw"></span>
</a>
<a target="_blank" href="http://linkedin.com" data-uk-tooltip="{pos:'bottom'}" title="Linkedin"   >
<span class="fa fa-linkedin fa-fw"></span>
</a>
<a target="_blank" href="http://dribbble.com" data-uk-tooltip="{pos:'bottom'}" title="Dribbble"   >
<span class="fa fa-dribbble fa-fw"></span>
</a>
</div>
</div>  </div>
</div>
</div>
</div>
</section>
</div>
<div class="g-container"> <header id="g-header">
<div          
         class="g-grid">
<div         
         class="g-block size-30">
<div class="g-content g-particle">
<a href="/templates/headlines" title="Headlines" rel="home" class="g-logo ">
<img src="images/logo.png" alt="Headlines" width="170" height="70" />
</a>
</div>
</div>
<div         
         class="g-block hidden-phone size-70">
<div class="g-content">
<div class="platform-content"><div class="moduletable ">
<div class="bannergroup">
<div class="banneritem">
<a
                            href="/templates/headlines/index.php/component/banners/click/1" target="_blank"
                            title="Header Banner">
<img
                                src="http://demo.inspiretheme.com/templates/headlines/images/Demo/elements/banner1.png"
                                alt="Header Banner" width="468" height="60" />
</a>
<div class="clr"></div>
</div>
</div>
</div></div>
</div>
</div>
</div>
</header>
</div>
<div class="g-container"> <section id="g-navigation" data-uk-sticky="">
<div          
         class="g-grid">
<div         
         class="g-block size-75">
<div class="g-content g-particle">
<nav class="g-main-nav" role="navigation" data-g-mobile-target data-g-hover-expand="true">
<ul class="g-toplevel">
<li class="g-menu-item g-menu-item-type-component g-menu-item-101 g-parent g-standard g-menu-item-link-parent ">
<a class="g-menu-item-container" href="/templates/headlines/index.php">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Home</span>
</span>
<span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
<ul class="g-dropdown g-inactive g-fade g-dropdown-right">
<li class="g-dropdown-column">
<div class="g-grid">
<div class="g-block size-100">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-125  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/homepage/home-1-default">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Home 1 (Default)</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-126  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/homepage/home-2">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Home 2</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-236  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/homepage/fullwidth-layout">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Fullwidth Layout</span>
</span>
</a>
</li>
</ul>
</div>
</div>
</li>
</ul>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-229 g-parent g-fullwidth g-menu-item-link-parent ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/business">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Business</span>
</span>
<span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
<ul class="g-dropdown g-inactive g-fade ">
<li class="g-dropdown-column">
<div class="g-grid">
<div class="g-block size-100">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-particle g-menu-item-business---particle-0vCDc  ">
<div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
<div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
<div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
<div class="uk-slider-container">
<ul class="uk-slideset uk-grid">
<li>
<div class="g-content-pro-item uk-overlay g-cat-business g-featured-article">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/business/10-vestibulum-faucibus-mollis-tel-massa-id-est">
<img src="images/business9.jpg" width="1000" height="500" alt="Vestibulum faucibus mollis tel massa id est dolor" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/business/10-vestibulum-faucibus-mollis-tel-massa-id-est">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-business">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/business/9-maecenas-facilisis-dapibus-lectus-in-metus">
<img src="images/business8.jpg" width="1000" height="500" alt="Maecenas facilisis dapibus lectus in metus odio" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/business/9-maecenas-facilisis-dapibus-lectus-in-metus">Maecenas facilisis dapibus lectus in metus odio</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-business">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/business/8-donec-pulvinar-nisi-vitae-odio-suscipit-sit-amet">
<img src="images/business7.jpg" width="1000" height="500" alt="Donec pulvinar nisi vitae odio suscipit sit amet lectu" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/business/8-donec-pulvinar-nisi-vitae-odio-suscipit-sit-amet">Donec pulvinar nisi vitae odio suscipit sit amet lectu</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-business">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/business/7-aliquam-sodales-quam-in-era-neque-porttitor">
<img src="images/business6.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/business/7-aliquam-sodales-quam-in-era-neque-porttitor">Aliquam sodales quam in era neque porttitor</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-business">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/business/6-proin-volutpat-tristique-diam-quis-euismod-sed">
<img src="images/business5.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/business/6-proin-volutpat-tristique-diam-quis-euismod-sed">Proin volutpat tristique diam quis euismod sed</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-business">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/business/5-class-aptent-taciti-socio-ad-litora-est-fringi">
<img src="images/business4.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/business/5-class-aptent-taciti-socio-ad-litora-est-fringi">Class aptent taciti socio ad litora est fringi</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
</ul>
</div>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
</div>
</div>
</div>
</div> </li>
</ul>
</div>
</div>
</li>
</ul>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-230 g-parent g-fullwidth g-menu-item-link-parent ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/lifestyle">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Lifestyle</span>
</span>
<span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
<ul class="g-dropdown g-inactive g-fade ">
<li class="g-dropdown-column">
<div class="g-grid">
<div class="g-block size-100">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-particle g-menu-item-lifestyle---particle-UsJxk  ">
<div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
<div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
<div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
<div class="uk-slider-container">
<ul class="uk-slideset uk-grid">
<li>
<div class="g-content-pro-item uk-overlay g-cat-lifestyle g-featured-article">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/lifestyle/22-proin-volutpat-tristique-diam-quis-euismod-sed-sit-amet">
<img src="images/lifestyle12.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed sit amet" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/lifestyle/22-proin-volutpat-tristique-diam-quis-euismod-sed-sit-amet">Proin volutpat tristique diam quis euismod sed sit amet</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-lifestyle">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/lifestyle/21-class-aptent-taciti-socio-ad-litora-est-fringi-mauris-orci">
<img src="images/lifestyle11.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi mauris orci" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/lifestyle/21-class-aptent-taciti-socio-ad-litora-est-fringi-mauris-orci">Class aptent taciti socio ad litora est fringi mauris orci</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-lifestyle">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/lifestyle/20-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eget-turpis">
<img src="images/lifestyle10.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eget turpis" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/lifestyle/20-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eget-turpis">Uisque ac gravida ligula nunc nisi risus ipsum eget turpis</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-lifestyle">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/lifestyle/19-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor">
<img src="images/lifestyle9.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/lifestyle/19-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-lifestyle">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/lifestyle/18-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">
<img src="images/lifestyle8.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh mauris" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/lifestyle/18-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">Etiam eu sapien at purus ultricies tempor nabh mauris</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-lifestyle">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/lifestyle/17-lorem-ipsum-dolor-sit-amet-conse-adipiscing">
<img src="images/lifestyle7.jpg" width="1000" height="500" alt="Lorem ipsum dolor sit amet, conse adipiscing" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/lifestyle/17-lorem-ipsum-dolor-sit-amet-conse-adipiscing">Lorem ipsum dolor sit amet, conse adipiscing</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
</ul>
</div>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
</div>
</div>
</div>
</div> </li>
</ul>
</div>
</div>
</li>
</ul>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-231 g-parent g-fullwidth g-menu-item-link-parent ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/technology">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Technology</span>
</span>
<span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
<ul class="g-dropdown g-inactive g-fade ">
<li class="g-dropdown-column">
<div class="g-grid">
<div class="g-block size-100">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-particle g-menu-item-technology---particle-6i1qf  ">
<div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
<div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
<div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
<div class="uk-slider-container">
<ul class="uk-slideset uk-grid">
<li>
<div class="g-content-pro-item uk-overlay g-cat-technology">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/technology/31-phasellus-eget-augue-est-fring-metus-felis-sit-amet">
<img src="images/technology9.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis sit amet" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/technology/31-phasellus-eget-augue-est-fring-metus-felis-sit-amet">Phasellus eget augue est fring metus felis sit amet</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-technology g-featured-article">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/technology/30-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-auctor-ante">
<img src="images/technology8.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh auctor" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/technology/30-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-auctor-ante">Etiam eu sapien at purus ultricies tempor nabh auctor</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-technology">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/technology/29-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus">
<img src="images/technology7.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor quis metus" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/technology/29-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus">Aliquam sodales quam in era neque porttitor quis metus</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-technology">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/technology/28-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">
<img src="images/technology6.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed posuere" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/technology/28-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">Proin volutpat tristique diam quis euismod sed posuere</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-technology">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/technology/27-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim">
<img src="images/technology5.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi non mi enim" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/technology/27-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim">Class aptent taciti socio ad litora est fringi non mi enim</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-technology">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/technology/26-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan">
<img src="images/technology4.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/technology/26-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
</ul>
</div>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
</div>
</div>
</div>
</div> </li>
</ul>
</div>
</div>
</li>
</ul>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-232 g-parent g-fullwidth g-menu-item-link-parent ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/fashion">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Fashion</span>
</span>
<span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
<ul class="g-dropdown g-inactive g-fade ">
<li class="g-dropdown-column">
<div class="g-grid">
<div class="g-block size-100">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-particle g-menu-item-fashion---particle-1Ex12  ">
<div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
<div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
<div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
<div class="uk-slider-container">
<ul class="uk-slideset uk-grid">
<li>
<div class="g-content-pro-item uk-overlay g-cat-fashion">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/fashion/41-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">
<img src="images/fashion9.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh mauris" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/fashion/41-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">Etiam eu sapien at purus ultricies tempor nabh mauris</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-fashion">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/fashion/40-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor">
<img src="images/fashion8.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/fashion/40-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-fashion g-featured-article">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/fashion/39-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan">
<img src="images/fashion7.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/fashion/39-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-fashion">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/fashion/38-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis">
<img src="images/fashion6.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi non mi enim" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/fashion/38-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis">Class aptent taciti socio ad litora est fringi non mi enim</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-fashion">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/fashion/37-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">
<img src="images/fashion5.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed posuere" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/fashion/37-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">Proin volutpat tristique diam quis euismod sed posuere</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-fashion">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/fashion/36-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus">
<img src="images/fashion4.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor quis metus" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/fashion/36-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus">Aliquam sodales quam in era neque porttitor quis metus</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
</ul>
</div>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
</div>
</div>
</div>
</div> </li>
</ul>
</div>
</div>
</li>
</ul>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-233 g-parent active g-fullwidth g-menu-item-link-parent ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/travel">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Travel</span>
</span>
<span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
<ul class="g-dropdown g-inactive g-fade ">
<li class="g-dropdown-column">
<div class="g-grid">
<div class="g-block size-100">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-particle g-menu-item-travel---particle-QRP9Y  ">
<div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
<div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
<div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
<div class="uk-slider-container">
<ul class="uk-slideset uk-grid">
<li>
<div class="g-content-pro-item uk-overlay g-cat-travel">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/travel/46-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">
<img src="images/travel9.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed posuere" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/46-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">Proin volutpat tristique diam quis euismod sed posuere</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-travel">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/travel/45-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis">
<img src="images/travel8.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi non mi enim" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/45-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis">Class aptent taciti socio ad litora est fringi non mi enim</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-travel">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/travel/44-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan">
<img src="images/travel7.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/44-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-travel">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/travel/43-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor">
<img src="images/travel6.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/43-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay g-cat-travel g-featured-article">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">
<img src="images/travel5.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultri tem nabh mauris orci" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">Etiam eu sapien at purus ultri tem nabh mauris orci</a></h4>
<div class="g-article-details details-">
</div>
</div>
</div>
</li>
</ul>
</div>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
</div>
</div>
</div>
</div> </li>
</ul>
</div>
</div>
</li>
</ul>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-150 g-parent g-fullwidth g-menu-item-link-parent ">
<a class="g-menu-item-container" href="#">
<span class="g-menu-item-content">
<span class="g-menu-item-title">More</span>
</span>
<span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
<ul class="g-dropdown g-inactive g-fade ">
<li class="g-dropdown-column">
<div class="g-grid">
<div class="g-block size-25">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-151  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/contact-us">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Contact Us</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-152  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/about-us">
<span class="g-menu-item-content">
<span class="g-menu-item-title">About Us</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-153  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/roksprocket-mosaic-default">
<span class="g-menu-item-content">
<span class="g-menu-item-title">RokSprocket Mosaic (Default)</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-154  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/roksprocket-mosaic-gallery">
<span class="g-menu-item-content">
<span class="g-menu-item-title">RokSprocket Mosaic (Gallery)</span>
</span>
</a>
</li>
</ul>
</div>
<div class="g-block size-25">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-168  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/search-results">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Search Results</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-176  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/member-login">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Member Login</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-178  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/registration-form">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Registration Form</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-181  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/pagessss">
<span class="g-menu-item-content">
<span class="g-menu-item-title">404 Page</span>
</span>
</a>
</li>
</ul>
</div>
<div class="g-block size-25">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-192  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/module-positions">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Layout Manager</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-193  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/menu-editor">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Menu Editor</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-194  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/particles">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Particles</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-197  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/uikit-framework">
<span class="g-menu-item-content">
<span class="g-menu-item-title">UIkit Framework</span>
</span>
</a>
</li>
</ul>
</div>
<div class="g-block size-25">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-198  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/scrollreveal-js">
<span class="g-menu-item-content">
<span class="g-menu-item-title">ScrollReveal.js</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-191  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/module-variations">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Module Variations</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-component g-menu-item-196  ">
<a class="g-menu-item-container" href="/templates/headlines/index.php/more/typography">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Typography</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-199  ">
<a class="g-menu-item-container" href="http://www.inspiretheme.com/documentation/joomla-templates/headlines" target="_blank">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Documentation</span>
</span>
</a>
</li>
</ul>
</div>
</div>
</li>
</ul>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-184 g-parent g-standard g-menu-item-link-parent presets-demo">
<a class="g-menu-item-container" href="#">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Presets</span>
</span>
<span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
<ul class="g-dropdown g-inactive g-fade g-dropdown-right">
<li class="g-dropdown-column">
<div class="g-grid">
<div class="g-block size-100">
<ul class="g-sublevel">
<li class="g-level-1 g-go-back">
<a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-185  preset1">
<a class="g-menu-item-container" href="/templates/headlines/?presets=preset1">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Preset 1 (Default)</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-186  preset2">
<a class="g-menu-item-container" href="/templates/headlines/?presets=preset2">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Preset 2</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-187  preset3">
<a class="g-menu-item-container" href="/templates/headlines/?presets=preset3">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Preset 3</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-188  preset4">
<a class="g-menu-item-container" href="/templates/headlines/?presets=preset4">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Preset 4</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-189  preset5">
<a class="g-menu-item-container" href="/templates/headlines/?presets=preset5">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Preset 5</span>
</span>
</a>
</li>
<li class="g-menu-item g-menu-item-type-url g-menu-item-190  preset6">
<a class="g-menu-item-container" href="/templates/headlines/?presets=preset6">
<span class="g-menu-item-content">
<span class="g-menu-item-title">Preset 6</span>
</span>
</a>
</li>
</ul>
</div>
</div>
</li>
</ul>
</li>
</ul>
</nav>
</div>
</div>
<div         
         class="g-block size-25">
<div class="g-content">
<div class="moduletable ">
<div class="g-particle">
<div class="g-search-login" >
<div class="g-buttons">
<div class="g-search-button">
<a id="header-search-toggle" href="/templates/headlines/" data-uk-tooltip title="Search">
<i class="fa fa-search"></i>
</a>
</div>
<div class="g-login-button">
<a href="#modal-login" data-uk-modal="{center:true}" data-uk-tooltip title="Login">
<i class="fa fa-user"></i>
</a>
</div>
<div class="g-offcanvas-button">
<a class="offcanvas-toggle-particle" data-offcanvas-toggle="" data-uk-tooltip title="Offcanvas">
<i class="fa fa-bars"></i>
</a>
</div>
</div>
<div id="header-search">
<div class="g-block">
<div class="g-content">
<a class="uk-close"></a>
<div class="moduletable">
<div class="search mod_search93">
<form action="/templates/headlines/index.php" method="post" class="form-inline">
<label for="mod-search-searchword" class="element-invisible">Search ...</label> <input autocomplete="off" name="searchword" id="mod-search-searchword" maxlength="200"  class="inputbox search-query" type="search" size="20" placeholder="Search ..." />   <input type="hidden" name="task" value="search" />
<input type="hidden" name="option" value="com_search" />
<input type="hidden" name="Itemid" value="101" />
</form>
</div>
</div>
</div>
</div>
</div>
<div id="modal-login" class="uk-modal">
<a class="uk-modal-close uk-close"></a>
<div class="uk-modal-dialog">
<div class="moduletabletitle-border">
<h3>Login To Your Account</h3>
<form action="http://demo.inspiretheme.com/templates/headlines/" method="post" id="login-form" class="form-inline">
<div class="userdata">
<div id="form-login-username" class="control-group">
<div class="controls">
<label for="modlgn-username" class="element-invisible">Username</label>
<input id="modlgn-username" type="text" name="username" tabindex="0" size="18" placeholder="Username" />
</div>
</div>
<div id="form-login-password" class="control-group">
<div class="controls">
<label for="modlgn-passwd" class="element-invisible">Password</label>
<input id="modlgn-passwd" type="password" name="password" tabindex="0" size="18" placeholder="Password" />
</div>
</div>
<div id="form-login-remember" class="control-group checkbox">
<label for="modlgn-remember" class="control-label">Remember Me</label> <input id="modlgn-remember" type="checkbox" name="remember" class="inputbox" value="yes"/>
</div>
<div id="form-login-submit" class="control-group">
<div class="controls">
<button type="submit" tabindex="0" name="Submit" class="btn btn-primary">Log in</button>
</div>
</div>
<ul class="unstyled">
<li>
<a href="/templates/headlines/index.php/more/registration-form">
<i class="fa fa-question-circle"></i>Create an account</a>
</li>
<li>
<a href="/templates/headlines/index.php/more/registration-form?view=remind">
<i class="fa fa-question-circle"></i>Forgot your username?</a>
</li>
<li>
<a href="/templates/headlines/index.php/more/registration-form?view=reset">
<i class="fa fa-question-circle"></i>Forgot your password?</a>
</li>
</ul>
<input type="hidden" name="option" value="com_users" />
<input type="hidden" name="task" value="user.login" />
<input type="hidden" name="return" value="aHR0cDovL2RlbW8uaW5zcGlyZXRoZW1lLmNvbS90ZW1wbGF0ZXMvaGVhZGxpbmVzLw==" />
<input type="hidden" name="0d14b02b3838edaccae7d7d9c3c6cf86" value="1" />   </div>
</form>
</div>
</div>
</div>
</div>
</div>  </div>
</div>
</div>
</div>
</section>
</div>
<div class="g-container"> <section id="g-breadcrumb">
<div          
         class="g-grid">
<div         
         class="g-block size-100">
<div class="g-content">
<div class="platform-content"><div class="moduletable ">
<ul itemscope itemtype="https://schema.org/BreadcrumbList" class="breadcrumb">
<li>
            You are here: &#160;
        </li>
<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
<a itemprop="item" href="/templates/headlines/index.php" class="pathway"><span itemprop="name">Home</span></a>
<span class="divider">
<img src="images/arrow.png" alt="" width="9" height="9" />    </span>
<meta itemprop="position" content="1">
</li>
<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="active">
<span itemprop="name">
                    Travel              </span>
<meta itemprop="position" content="2">
</li>
</ul>
</div></div>
</div>
</div>
</div>
</section>
</div>
<div class="g-container"> <section id="g-system-messages">
<div          
         class="g-grid">
<div         
         class="g-block size-100">
<div class="g-system-messages">
</div>
</div>
</div>
</section>
</div>
<div class="g-container"> <section id="g-container-main" class="g-wrapper">
<div          
         class="g-grid">
<div         
         class="g-block size-67">
<section id="g-mainbody">
<div          
         class="g-grid">
<div         
         class="g-block size-100">
<div class="g-content">
<div class="platform-content row-fluid"><div class="span12"><div class="blog" itemscope itemtype="http://schema.org/Blog">
<div class="items-leading clearfix">
<article class="item leading-0"
                    itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
<div class="pull-none item-image">
<a href="/templates/headlines/index.php/travel/46-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut"><img src="images/travel9.jpg" alt="" itemprop="thumbnailUrl" width="1000" height="500" /></a>
</div>
<div class="g-article-header">
<div class="page-header">
<h2 itemprop="name">
<a href="/templates/headlines/index.php/travel/46-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut" itemprop="url">
                        Proin volutpat tristique diam quis euismod sed posuere                  </a>
</h2>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
                                    Details                         </dt>
<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
<span itemprop="name" data-uk-tooltip title="Written by "><i class="fa fa-user"></i>Super User</span>   </dd>
<dd class="category-name">
<span data-uk-tooltip title="Article Category"><i class="fa fa-folder-open-o"></i><a href="/templates/headlines/index.php/travel" itemprop="genre">Travel</a></span>    </dd>
<dd class="published">
<time datetime="2016-09-09T16:49:00+00:00" itemprop="datePublished" data-uk-tooltip title="Published Date">
<i class="fa fa-clock-o"></i>09 September 2016  </time>
</dd>
<dd class="hits">
<meta itemprop="interactionCount" content="UserPageVisits:57" />
<span data-uk-tooltip title="Hits: "><i class="fa fa-eye"></i>57</span>
</dd>   </dl>
</div>
<p>Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam eu sapien at purus ultricies tempor. Suspendisse interdum, nibh vel vulputate tristique, massa tellus sodales mauris, ac molestie massa magna ultrices odio. Aliquam blandit mollis mauris at pharetra. Morbi libero enim, interdum sit amet interdum eget, imperdiet ut orci. Sed sit amet accumsan est. Morbi nunc turpis, vestibulum at tincidunt ac, semper nec justo. Vivamus quis placerat nisi. Vivamus mauris tortor, pellentesque a adipiscing sed, tempus vel enim.</p>
<p class="readmore">
<a class="btn" href="/templates/headlines/index.php/travel/46-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut" itemprop="url">
<span class="icon-chevron-right"></span>
        Read more ...   </a>
</p>
</article>
<article class="item leading-1"
                    itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
<div class="pull-none item-image">
<a href="/templates/headlines/index.php/travel/45-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis"><img src="images/travel8.jpg" alt="" itemprop="thumbnailUrl" width="1000" height="500" /></a>
</div>
<div class="g-article-header">
<div class="page-header">
<h2 itemprop="name">
<a href="/templates/headlines/index.php/travel/45-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis" itemprop="url">
                        Class aptent taciti socio ad litora est fringi non mi enim                  </a>
</h2>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
                                    Details                         </dt>
<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
<span itemprop="name" data-uk-tooltip title="Written by "><i class="fa fa-user"></i>Super User</span>   </dd>
<dd class="category-name">
<span data-uk-tooltip title="Article Category"><i class="fa fa-folder-open-o"></i><a href="/templates/headlines/index.php/travel" itemprop="genre">Travel</a></span>    </dd>
<dd class="published">
<time datetime="2016-09-08T16:48:55+00:00" itemprop="datePublished" data-uk-tooltip title="Published Date">
<i class="fa fa-clock-o"></i>08 September 2016  </time>
</dd>
<dd class="hits">
<meta itemprop="interactionCount" content="UserPageVisits:63" />
<span data-uk-tooltip title="Hits: "><i class="fa fa-eye"></i>63</span>
</dd>   </dl>
</div>
<p>Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam eu sapien at purus ultricies tempor. Suspendisse interdum, nibh vel vulputate tristique, massa tellus sodales mauris, ac molestie massa magna ultrices odio. Aliquam blandit mollis mauris at pharetra. Morbi libero enim, interdum sit amet interdum eget, imperdiet ut orci. Sed sit amet accumsan est. Morbi nunc turpis, vestibulum at tincidunt ac, semper nec justo. Vivamus quis placerat nisi. Vivamus mauris tortor, pellentesque a adipiscing sed, tempus vel enim.</p>
<p class="readmore">
<a class="btn" href="/templates/headlines/index.php/travel/45-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis" itemprop="url">
<span class="icon-chevron-right"></span>
        Read more ...   </a>
</p>
</article>
<article class="item leading-2"
                    itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
<div class="pull-none item-image">
<a href="/templates/headlines/index.php/travel/44-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan"><img src="images/travel7.jpg" alt="" itemprop="thumbnailUrl" width="1000" height="500" /></a>
</div>
<div class="g-article-header">
<div class="page-header">
<h2 itemprop="name">
<a href="/templates/headlines/index.php/travel/44-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan" itemprop="url">
                        Uisque ac gravida ligula nunc nisi risus ipsum eros                 </a>
</h2>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
                                    Details                         </dt>
<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
<span itemprop="name" data-uk-tooltip title="Written by "><i class="fa fa-user"></i>Super User</span>   </dd>
<dd class="category-name">
<span data-uk-tooltip title="Article Category"><i class="fa fa-folder-open-o"></i><a href="/templates/headlines/index.php/travel" itemprop="genre">Travel</a></span>    </dd>
<dd class="published">
<time datetime="2016-09-07T16:48:05+00:00" itemprop="datePublished" data-uk-tooltip title="Published Date">
<i class="fa fa-clock-o"></i>07 September 2016  </time>
</dd>
<dd class="hits">
<meta itemprop="interactionCount" content="UserPageVisits:37" />
<span data-uk-tooltip title="Hits: "><i class="fa fa-eye"></i>37</span>
</dd>   </dl>
</div>
<p>Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam eu sapien at purus ultricies tempor. Suspendisse interdum, nibh vel vulputate tristique, massa tellus sodales mauris, ac molestie massa magna ultrices odio. Aliquam blandit mollis mauris at pharetra. Morbi libero enim, interdum sit amet interdum eget, imperdiet ut orci. Sed sit amet accumsan est. Morbi nunc turpis, vestibulum at tincidunt ac, semper nec justo. Vivamus quis placerat nisi. Vivamus mauris tortor, pellentesque a adipiscing sed, tempus vel enim.</p>
<p class="readmore">
<a class="btn" href="/templates/headlines/index.php/travel/44-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan" itemprop="url">
<span class="icon-chevron-right"></span>
        Read more ...   </a>
</p>
</article>
<article class="item leading-3"
                    itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
<div class="pull-none item-image">
<a href="/templates/headlines/index.php/travel/43-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor"><img src="images/travel6.jpg" alt="" itemprop="thumbnailUrl" width="1000" height="500" /></a>
</div>
<div class="g-article-header">
<div class="page-header">
<h2 itemprop="name">
<a href="/templates/headlines/index.php/travel/43-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor" itemprop="url">
                        Phasellus eget augue est fring metus felis ipsum dolor                  </a>
</h2>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
                                    Details                         </dt>
<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
<span itemprop="name" data-uk-tooltip title="Written by "><i class="fa fa-user"></i>Super User</span>   </dd>
<dd class="category-name">
<span data-uk-tooltip title="Article Category"><i class="fa fa-folder-open-o"></i><a href="/templates/headlines/index.php/travel" itemprop="genre">Travel</a></span>    </dd>
<dd class="published">
<time datetime="2016-09-06T16:47:04+00:00" itemprop="datePublished" data-uk-tooltip title="Published Date">
<i class="fa fa-clock-o"></i>06 September 2016  </time>
</dd>
<dd class="hits">
<meta itemprop="interactionCount" content="UserPageVisits:36" />
<span data-uk-tooltip title="Hits: "><i class="fa fa-eye"></i>36</span>
</dd>   </dl>
</div>
<p>Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam eu sapien at purus ultricies tempor. Suspendisse interdum, nibh vel vulputate tristique, massa tellus sodales mauris, ac molestie massa magna ultrices odio. Aliquam blandit mollis mauris at pharetra. Morbi libero enim, interdum sit amet interdum eget, imperdiet ut orci. Sed sit amet accumsan est. Morbi nunc turpis, vestibulum at tincidunt ac, semper nec justo. Vivamus quis placerat nisi. Vivamus mauris tortor, pellentesque a adipiscing sed, tempus vel enim.</p>
<p class="readmore">
<a class="btn" href="/templates/headlines/index.php/travel/43-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor" itemprop="url">
<span class="icon-chevron-right"></span>
        Read more ...   </a>
</p>
</article>
<article class="item leading-4"
                    itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
<div class="pull-none item-image">
<a href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci"><img src="images/travel5.jpg" alt="" itemprop="thumbnailUrl" width="1000" height="500" /></a>
</div>
<div class="g-article-header">
<div class="page-header">
<h2 itemprop="name">
<a href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci" itemprop="url">
                        Etiam eu sapien at purus ultri tem nabh mauris orci                 </a>
</h2>
</div>
<dl class="article-info muted">
<dt class="article-info-term">
                                    Details                         </dt>
<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
<span itemprop="name" data-uk-tooltip title="Written by "><i class="fa fa-user"></i>Super User</span>   </dd>
<dd class="category-name">
<span data-uk-tooltip title="Article Category"><i class="fa fa-folder-open-o"></i><a href="/templates/headlines/index.php/travel" itemprop="genre">Travel</a></span>    </dd>
<dd class="published">
<time datetime="2016-09-05T16:46:10+00:00" itemprop="datePublished" data-uk-tooltip title="Published Date">
<i class="fa fa-clock-o"></i>05 September 2016  </time>
</dd>
<dd class="hits">
<meta itemprop="interactionCount" content="UserPageVisits:45" />
<span data-uk-tooltip title="Hits: "><i class="fa fa-eye"></i>45</span>
</dd>   </dl>
</div>
<p>Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam eu sapien at purus ultricies tempor. Suspendisse interdum, nibh vel vulputate tristique, massa tellus sodales mauris, ac molestie massa magna ultrices odio. Aliquam blandit mollis mauris at pharetra. Morbi libero enim, interdum sit amet interdum eget, imperdiet ut orci. Sed sit amet accumsan est. Morbi nunc turpis, vestibulum at tincidunt ac, semper nec justo. Vivamus quis placerat nisi. Vivamus mauris tortor, pellentesque a adipiscing sed, tempus vel enim.</p>
<p class="readmore">
<a class="btn" href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci" itemprop="url">
<span class="icon-chevron-right"></span>
        Read more ...   </a>
</p>
</article>
</div><!-- end items-leading -->
</div></div></div>
</div>
</div>
</div>
</section>
</div>
<div         
         class="g-block size-33">
<aside id="g-aside">
<div          
         class="g-grid">
<div         
         class="g-block size-100">
<div class="g-content">
<div class="platform-content"><div class="moduletable  box2">
<h3 class="g-title"><span>Main Menu</span></h3>
<ul class="nav menu" id=" box2">
<li class="item-101 default parent"><a href="/templates/headlines/index.php" >Home</a></li><li class="item-229"><a href="/templates/headlines/index.php/business" >Business</a></li><li class="item-230"><a href="/templates/headlines/index.php/lifestyle" >Lifestyle</a></li><li class="item-231"><a href="/templates/headlines/index.php/technology" >Technology</a></li><li class="item-232"><a href="/templates/headlines/index.php/fashion" >Fashion</a></li><li class="item-233 current active"><a href="/templates/headlines/index.php/travel" >Travel</a></li><li class="item-150 parent"><a href="#" >More</a></li><li class="item-184 parent"><a href="#" >Presets</a></li></ul>
</div></div><div class="platform-content"><div class="moduletable box2">
<h3 class="g-title"><span>Login Form</span></h3>
<form action="http://demo.inspiretheme.com/templates/headlines/index.php/travel" method="post" id="login-form" class="form-inline">
<div class="userdata">
<div id="form-login-username" class="control-group">
<div class="controls">
<label for="modlgn-username" class="element-invisible">Username</label>
<input id="modlgn-username" type="text" name="username" tabindex="0" size="18" placeholder="Username" />
</div>
</div>
<div id="form-login-password" class="control-group">
<div class="controls">
<label for="modlgn-passwd" class="element-invisible">Password</label>
<input id="modlgn-passwd" type="password" name="password" tabindex="0" size="18" placeholder="Password" />
</div>
</div>
<div id="form-login-remember" class="control-group checkbox">
<label for="modlgn-remember" class="control-label">Remember Me</label> <input id="modlgn-remember" type="checkbox" name="remember" class="inputbox" value="yes"/>
</div>
<div id="form-login-submit" class="control-group">
<div class="controls">
<button type="submit" tabindex="0" name="Submit" class="btn btn-primary">Log in</button>
</div>
</div>
<ul class="unstyled">
<li>
<a href="/templates/headlines/index.php/more/registration-form">
<i class="fa fa-question-circle"></i>Create an account</a>
</li>
<li>
<a href="/templates/headlines/index.php/more/registration-form?view=remind">
<i class="fa fa-question-circle"></i>Forgot your username?</a>
</li>
<li>
<a href="/templates/headlines/index.php/more/registration-form?view=reset">
<i class="fa fa-question-circle"></i>Forgot your password?</a>
</li>
</ul>
<input type="hidden" name="option" value="com_users" />
<input type="hidden" name="task" value="user.login" />
<input type="hidden" name="return" value="aHR0cDovL2RlbW8uaW5zcGlyZXRoZW1lLmNvbS90ZW1wbGF0ZXMvaGVhZGxpbmVzL2luZGV4LnBocC90cmF2ZWw=" />
<input type="hidden" name="a69fcce922feaf41360cd8f0a7900845" value="1" />   </div>
</form>
</div></div><div class="platform-content"><div class="moduletable box2">
<h3 class="g-title"><span>Who's Online</span></h3>
<p>We have 7&#160;guests and no members online</p>
</div></div>
</div>
</div>
</div>
</aside>
</div>
</div>
</section>
</div>
<div class="g-container"> <footer id="g-footer">
<div          
         class="g-grid">
<div         
         class="g-block size-33">
<div class="g-content">
<div class="moduletable ">
<h3 class="g-title"><span>About Us</span></h3>
<div class="g-particle">
                                    Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. 
            
    </div>  </div><div class="moduletable ">
<div class="g-particle">
<div class="g-contacts uk-margin-top" >
<div class="g-contacts-item" >
<span class="g-contacts-icon fa fa-home"></span>
<span class="g-contact-value">64184 Vincent Place</span>
</div>
<div class="g-contacts-item" >
<span class="g-contacts-icon fa fa-phone"></span>
<span class="g-contact-value">0044 889 555 432</span>
</div>
<div class="g-contacts-item" >
<span class="g-contacts-icon fa fa-envelope-o"></span>
<span class="g-contact-value">office@headlines.com</span>
</div>
</div>
</div>  </div>
</div>
</div>
<div         
         class="g-block size-34">
<div class="g-content">
<div class="moduletable ">
<h3 class="g-title"><span>Recent News</span></h3>
<div class="g-particle">
<div class="g-news-pro" >
<div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
<div class="uk-slider-container">
<div class="uk-slideset uk-grid">
<div class="g-news-pro-page">
<div class="uk-grid uk-grid-width-1-1">
<div class="g-news-pro-item horizontal g-cat-business g-featured-article">
<div class="g-news-pro-image image-link" style="background-image: url(images/business9.jpg); width: 120px; height: 75px;">
<a href="/templates/headlines/index.php/business/10-vestibulum-faucibus-mollis-tel-massa-id-est"></a>
</div>
<div class="g-info-container">
<h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/business/10-vestibulum-faucibus-mollis-tel-massa-id-est">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
<div class="g-article-details details-show">
<span class="g-article-date"><i class="fa fa-clock-o"></i>September 15</span>
<span class="g-article-category">
<i class="fa fa-folder-open"></i>Business                                   </span>
</div>
</div>
</div>
</div>
<div class="uk-grid uk-grid-width-1-1">
<div class="g-news-pro-item horizontal g-cat-lifestyle g-featured-article">
<div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle12.jpg); width: 120px; height: 75px;">
<a href="/templates/headlines/index.php/lifestyle/22-proin-volutpat-tristique-diam-quis-euismod-sed-sit-amet"></a>
</div>
<div class="g-info-container">
<h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/22-proin-volutpat-tristique-diam-quis-euismod-sed-sit-amet">Proin volutpat tristique diam quis euismod sed...</a></h4>
<div class="g-article-details details-show">
<span class="g-article-date"><i class="fa fa-clock-o"></i>September 14</span>
<span class="g-article-category">
<i class="fa fa-folder-open"></i>Lifestyle                                  </span>
</div>
</div>
</div>
</div>
</div>
<div class="g-news-pro-page">
<div class="uk-grid uk-grid-width-1-1">
<div class="g-news-pro-item horizontal g-cat-lifestyle">
<div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle11.jpg); width: 120px; height: 75px;">
<a href="/templates/headlines/index.php/lifestyle/21-class-aptent-taciti-socio-ad-litora-est-fringi-mauris-orci"></a>
</div>
<div class="g-info-container">
<h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/21-class-aptent-taciti-socio-ad-litora-est-fringi-mauris-orci">Class aptent taciti socio ad litora est fringi...</a></h4>
<div class="g-article-details details-show">
<span class="g-article-date"><i class="fa fa-clock-o"></i>September 13</span>
<span class="g-article-category">
<i class="fa fa-folder-open"></i>Lifestyle                                  </span>
</div>
</div>
</div>
</div>
<div class="uk-grid uk-grid-width-1-1">
<div class="g-news-pro-item horizontal g-cat-fashion">
<div class="g-news-pro-image image-link" style="background-image: url(images/fashion9.jpg); width: 120px; height: 75px;">
<a href="/templates/headlines/index.php/fashion/41-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci"></a>
</div>
<div class="g-info-container">
<h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/fashion/41-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">Etiam eu sapien at purus ultricies tempor nabh...</a></h4>
<div class="g-article-details details-show">
<span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
<span class="g-article-category">
<i class="fa fa-folder-open"></i>Fashion                                    </span>
</div>
</div>
</div>
</div>
</div>
<div class="g-news-pro-page">
<div class="uk-grid uk-grid-width-1-1">
<div class="g-news-pro-item horizontal g-cat-lifestyle">
<div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle10.jpg); width: 120px; height: 75px;">
<a href="/templates/headlines/index.php/lifestyle/20-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eget-turpis"></a>
</div>
<div class="g-info-container">
<h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/20-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eget-turpis">Uisque ac gravida ligula nunc nisi risus ipsum...</a></h4>
<div class="g-article-details details-show">
<span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
<span class="g-article-category">
<i class="fa fa-folder-open"></i>Lifestyle                                  </span>
</div>
</div>
</div>
</div>
<div class="uk-grid uk-grid-width-1-1">
<div class="g-news-pro-item horizontal g-cat-business">
<div class="g-news-pro-image image-link" style="background-image: url(images/business8.jpg); width: 120px; height: 75px;">
<a href="/templates/headlines/index.php/business/9-maecenas-facilisis-dapibus-lectus-in-metus"></a>
</div>
<div class="g-info-container">
<h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/business/9-maecenas-facilisis-dapibus-lectus-in-metus">Maecenas facilisis dapibus lectus in metus odio</a></h4>
<div class="g-article-details details-show">
<span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
<span class="g-article-category">
<i class="fa fa-folder-open"></i>Business                                   </span>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="g-news-pro-nav">
<div class="g-news-pro-arrows">
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
<a href="/templates/headlines/" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
</div>
</div>
</div>
</div>
</div>  </div>
</div>
</div>
<div         
         class="g-block size-33">
<div class="g-content">
<div class="platform-content"><div class="moduletable ">
<h3 class="g-title"><span>Newsletter</span></h3>
<div class="acymailing_module" id="acymailing_module_formAcymailing52401">
<div class="acymailing_fulldiv" id="acymailing_fulldiv_formAcymailing52401"  >
<form id="formAcymailing52401" action="/templates/headlines/index.php/travel" onsubmit="return submitacymailingform('optin','formAcymailing52401')" method="post" name="formAcymailing52401"  >
<div class="acymailing_module_form" >
<div class="acymailing_introtext">Stay tuned with our latest news!</div>    <table class="acymailing_form">
<tr>
<td class="acyfield_name acy_requiredField">
<input id="user_name_formAcymailing52401"  onfocus="if(this.value == 'Name') this.value = '';" onblur="if(this.value=='') this.value='Name';" class="inputbox" type="text" name="user[name]" style="width:100%" value="Name" title="Name"/>
</td> </tr><tr> <td class="acyfield_email acy_requiredField">
<input id="user_email_formAcymailing52401"  onfocus="if(this.value == 'E-mail') this.value = '';" onblur="if(this.value=='') this.value='E-mail';" class="inputbox" type="text" name="user[email]" style="width:100%" value="E-mail" title="E-mail"/>
</td> </tr><tr>
<td  class="acysubbuttons">
<input class="button subbutton btn btn-primary" type="submit" value="Subscribe" name="Submit" onclick="try{ return submitacymailingform('optin','formAcymailing52401'); }catch(err){alert('The form could not be submitted '+err);return false;}"/>
</td>
</tr>
</table>
<input type="hidden" name="ajax" value="0" />
<input type="hidden" name="acy_source" value="module_211" />
<input type="hidden" name="ctrl" value="sub"/>
<input type="hidden" name="task" value="notask"/>
<input type="hidden" name="redirect" value="http%3A%2F%2Fdemo.inspiretheme.com%2Ftemplates%2Fheadlines%2Findex.php%2Ftravel"/>
<input type="hidden" name="redirectunsub" value="http%3A%2F%2Fdemo.inspiretheme.com%2Ftemplates%2Fheadlines%2Findex.php%2Ftravel"/>
<input type="hidden" name="option" value="com_acymailing"/>
<input type="hidden" name="hiddenlists" value="1"/>
<input type="hidden" name="acyformname" value="formAcymailing52401" />
</div>
</form>
</div>
</div>
</div></div>
</div>
</div>
</div>
</footer>
</div>
<div class="g-container"> <section id="g-copyright">
<div          
         class="g-grid">
<div         
         class="g-block size-50">
<div class="g-content">
<div class="moduletable ">
<div class="g-particle">
<div class="g-branding branding">
    © Headlines. Designed by <a href="http://inspiretheme.com">InspireTheme</a>.
</div>
</div>  </div>
</div>
</div>
<div         
         class="g-block size-50">
<div class="g-content">
<div class="moduletable ">
<div class="g-particle">
<div class="g-social" >
<a target="_blank" href="http://facebook.com" data-uk-tooltip title="Facebook"   >
<span class="fa fa-facebook"></span>
</a>
<a target="_blank" href="http://twitter.com" data-uk-tooltip title="Twitter"   >
<span class="fa fa-twitter"></span>
</a>
<a target="_blank" href="https://plus.google.com/" data-uk-tooltip title="Google+"   >
<span class="fa fa-google-plus"></span>
</a>
<a target="_blank" href="http://linkedin.com" data-uk-tooltip title="Linkedin"   >
<span class="fa fa-linkedin"></span>
</a>
<a target="_blank" href="http://dribbble.com" data-uk-tooltip title="Dribbble"   >
<span class="fa fa-dribbble"></span>
</a>
</div>
</div>  </div>
</div>
</div>
</div>
</section>
</div>
</section>
<div class="g-container"> <section id="g-to-top">
<div          
         class="g-grid">
<div         
         class="g-block size-100">
<div class="g-content g-particle">
<div class=" g-particle">
<div class="g-totop style1">
<a href="#" id="g-totop-button" rel="nofollow" data-uk-smooth-scroll>
<i class="fa fa-angle-up"></i> </a>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>
<script type="application/javascript" src="js/library.js"></script>
</body>
</html>