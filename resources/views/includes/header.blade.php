<section id="g-top">
    <div class="g-container">
        <div class="g-grid">
            <div class="g-block size-60">
                <div class="g-content">
                    <div class="moduletable ">
                        <div class="g-particle">
                            <nav class="g-main-nav" role="navigation" data-g-hover-expand="true">
                                <ul class="g-toplevel">
                                    <li class="g-menu-item g-menu-item-type-url g-menu-item-128 g-standard  ">
                                    <a class="g-menu-item-container" href="#">
                                    <span class="g-menu-item-content">
                                    <span class="g-menu-item-title">Advertise</span>
                                    </span>
                                    </a>
                                    </li>
                                    <li class="g-menu-item g-menu-item-type-url g-menu-item-129 g-standard  ">
                                        <a class="g-menu-item-container" href="#">
                                            <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">Contact Us</span>
                                            </span>
                                        </a>
                                    </li>
                                    <li class="g-menu-item g-menu-item-type-url g-menu-item-130 g-standard  ">
                                        <a class="g-menu-item-container" href="#">
                                            <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">Write For Us</span>
                                            </span>
                                        </a>
                                    </li>
                                    <li class="g-menu-item g-menu-item-type-url g-menu-item-131 g-parent g-standard g-menu-item-link-parent ">
                                        <a class="g-menu-item-container" href="#">
                                            <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">Dropdown</span>
                                            </span>
                                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span>
                                        </a>
                                        <ul class="g-dropdown g-inactive g-fade g-dropdown-right">
                                            <li class="g-dropdown-column">
                                                <div class="g-grid">
                                                    <div class="g-block size-100">
                                                        <ul class="g-sublevel">
                                                            <li class="g-level-1 g-go-back">
                                                                <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                                            </li>
                                                            <li class="g-menu-item g-menu-item-type-url g-menu-item-132  ">
                                                                <a class="g-menu-item-container" href="#">
                                                                    <span class="g-menu-item-content">
                                                                        <span class="g-menu-item-title">Subitem 1</span>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                            <li class="g-menu-item g-menu-item-type-url g-menu-item-133  ">
                                                                <a class="g-menu-item-container" href="#">
                                                                    <span class="g-menu-item-content">
                                                                        <span class="g-menu-item-title">Subitem 2</span>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                            <li class="g-menu-item g-menu-item-type-url g-menu-item-134  ">
                                                                <a class="g-menu-item-container" href="#">
                                                                    <span class="g-menu-item-content">
                                                                        <span class="g-menu-item-title">Subitem 3</span>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="g-block hidden-phone size-40">
                <div class="g-content">
                    <div class="moduletable ">
                        <div class="g-particle">
                            <div class="g-social" >
                                <a target="_blank" href="http://facebook.com" data-uk-tooltip="{pos:'bottom'}" title="Facebook"   >
                                    <span class="fa fa-facebook fa-fw"></span>
                                </a>
                                <a target="_blank" href="http://twitter.com" data-uk-tooltip="{pos:'bottom'}" title="Twitter"   >
                                    <span class="fa fa-twitter fa-fw"></span>
                                </a>
                                <a target="_blank" href="https://plus.google.com/" data-uk-tooltip="{pos:'bottom'}" title="Google+"   >
                                    <span class="fa fa-google-plus fa-fw"></span>
                                </a>
                                <a target="_blank" href="http://linkedin.com" data-uk-tooltip="{pos:'bottom'}" title="Linkedin"   >
                                    <span class="fa fa-linkedin fa-fw"></span>
                                </a>
                                <a target="_blank" href="http://dribbble.com" data-uk-tooltip="{pos:'bottom'}" title="Dribbble"   >
                                    <span class="fa fa-dribbble fa-fw"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<header id="g-header">
    <div class="g-container">
        <div class="g-grid">
            <div class="g-block size-30">
                <div class="g-content g-particle">
                    <a href="/" title="Headlines" rel="home" class="g-logo ">
                        <img src="images/logo.png" alt="Headlines" width="170" height="70" /></a>
                </div>
            </div>
            <div class="g-block hidden-phone size-70">
                <div class="g-content">
                    <div class="platform-content">
                        <div class="moduletable ">
                            <div class="bannergroup">
                                <div class="banneritem">
                                <a href="/" target="_blank" title="Header Banner">
                                    <img src="images/banner1.png" alt="Header Banner" width="468" height="60" />
                                </a>
                                <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>