<div class="g-container"> 
    <section id="g-breadcrumb">
        <div class="g-grid">
            <div class="g-block size-100">
                <div class="g-content">
                    <div class="platform-content">
                        <div class="moduletable ">
                            <ul itemscope="" itemtype="https://schema.org/BreadcrumbList" class="breadcrumb">
                                <li>You are here: &nbsp;</li>
                                <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
                                    <a itemprop="item" href="{{ asset('/') }}" class="pathway"><span itemprop="name">Home</span></a>
                                    <span class="divider"><img src="images/arrow.png" alt="" width="9" height="9">  </span>
                                    <meta itemprop="position" content="1">
                                </li>
                                <li itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem" class="active">
                                    <span itemprop="name">Business</span>
                                    <meta itemprop="position" content="2">
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>