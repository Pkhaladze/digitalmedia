<section id="g-showcase">
    <div class="g-container">
        <div class="g-grid">
            <div class="g-block size-100">
                <div class="g-content">
                    <div class="moduletable ">
                        <div class="g-particle">
                            <div class="g-top-news style1 g-inline_1209834220 gutter-disabled" >
                                <div class="g-top-news-container clearfix">
                                    <div class="g-top-news-item g-top-news-main g-tn1" style="height: 450px;">
                                        <div class="g-top-news-item-inner" style="background-image: url(images/business9.jpg);">
                                            <div class="g-top-news-item-image">
                                                <a href="##"></a>
                                            </div>
                                            <div class="g-top-news-item-info">
                                                <span class="g-article-category">
                                                    <a href="#/business"><span class="g-cat-business">Business</span></a>
                                                </span>
                                                <h4 class="g-article-title"><a href="##">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
                                                <div class="g-article-details">
                                                    <span class="g-article-date"><i class="fa fa-clock-o"></i>15 September</span>
                                                </div>
                                                <div class="g-article-text">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi...</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="g-top-news-item g-top-news-secondary g-tn2" style="height: 225px;">
                                        <div class="g-top-news-item-inner" style="background-image: url(images/lifestyle12.jpg);">
                                            <div class="g-top-news-item-image"><a href="#"></a></div>
                                            <div class="g-top-news-item-info">
                                                <span class="g-article-category">
                                                    <a href="#"><span class="g-cat-lifestyle">Lifestyle</span></a>
                                                </span>
                                                <h4 class="g-article-title">
                                                    <a href="#">Proin volutpat tristique diam quis euismod sed sit...</a>
                                                </h4>
                                                <div class="g-article-details">
                                                    <span class="g-article-date"><i class="fa fa-clock-o"></i>14 September</span>
                                                </div>
                                                <div class="g-article-text">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi...</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="g-top-news-item g-top-news-secondary g-tn3" style="height: 225px;">
                                        <div class="g-top-news-item-inner" style="background-image: url(images/fashion7.jpg);">
                                            <div class="g-top-news-item-image"><a href="#"></a></div>
                                            <div class="g-top-news-item-info">
                                                <span class="g-article-category">
                                                    <a href="#/fashion">
                                                        <span class="g-cat-fashion">Fashion</span>
                                                    </a>
                                                </span>
                                                <h4 class="g-article-title">
                                                    <a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eros</a>
                                                </h4>
                                                <div class="g-article-details">
                                                    <span class="g-article-date"><i class="fa fa-clock-o"></i>10 September</span>
                                                </div>
                                                <div class="g-article-text">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi...</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="g-top-news-item g-top-news-secondary g-tn4" style="height: 225px;">
                                        <div class="g-top-news-item-inner" style="background-image: url(images/technology8.jpg);">
                                            <div class="g-top-news-item-image">
                                                <a href="#"></a>
                                            </div>
                                            <div class="g-top-news-item-info">
                                                <span class="g-article-category">
                                                    <a href="#"><span class="g-cat-technology">Technology</span></a>
                                                </span>
                                                <h4 class="g-article-title">
                                                    <a href="#">Etiam eu sapien at purus ultricies tempor nabh auctor</a>
                                                </h4>
                                                <div class="g-article-details">
                                                    <span class="g-article-date"><i class="fa fa-clock-o"></i>10 September</span>
                                                </div>
                                                <div class="g-article-text">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi...</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="g-top-news-item g-top-news-secondary g-tn5" style="height: 225px;">
                                        <div class="g-top-news-item-inner" style="background-image: url(images/travel5.jpg);">
                                            <div class="g-top-news-item-image">
                                                <a href="#"></a>
                                            </div>
                                            <div class="g-top-news-item-info">
                                                <span class="g-article-category">
                                                    <a href="/templates/headlines/index.php/travel">
                                                        <span class="g-cat-travel">Travel</span></a>
                                                </span>
                                                <h4 class="g-article-title">
                                                    <a href="#">Etiam eu sapien at purus ultri tem nabh mauris orci</a></h4>
                                                <div class="g-article-details">
                                                    <span class="g-article-date"><i class="fa fa-clock-o"></i>05 September</span>
                                                </div>
                                                <div class="g-article-text">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi...</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="g-system-messages">
    <div class="g-container">
        <div class="g-grid">
            <div class="g-block size-100">
                <div class="g-system-messages"></div>
            </div>
        </div>
    </div>
</section>