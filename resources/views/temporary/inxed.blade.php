
<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8" />
    <title>Fullwidth Layout</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
<body class="gantry site com_gantry5 view-custom no-layout no-task dir-ltr itemid-236 outline-26 g-offcanvas-left g-default g-style-preset1">
<div id="g-offcanvas"  data-g-offcanvas-swipe="0" data-g-offcanvas-css3="1">
    <div class="g-grid">
        <div class="g-block size-100">
            <div class="g-content g-particle">
                <div id="g-mobilemenu-container" data-g-menu-breakpoint="60rem"></div>
            </div>
        </div>
    </div>
    <div class="g-grid">
        <div class="g-block size-100">
            <div class="g-content">
                <div class="platform-content"><div class="moduletable hidden-phone hidden-tablet">
                    <h3 class="g-title"><span>Offcanvas Section</span></h3>
                    <div class="customhidden-phone hidden-tablet"  >
                        <p>You can publish whatever you want in the Offcanvas Section. It can be any module or particle.</p>
                        <p>By default, the available module positions are <strong>offcanvas-a</strong> and <strong>offcanvas-b </strong>but you can add as many module positions as you want from the Layout Manager.</p>
                        <p>You can also add the <strong>hidden-phone</strong> module class suffix to your modules so they do not appear in the Offcanvas Section when the site is loaded on a mobile device.</p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="g-page-surround">
    <div class="g-offcanvas-hide g-offcanvas-toggle" data-offcanvas-toggle><i class="fa fa-fw fa-bars"></i></div>
    <section id="g-container-site" class="g-wrapper">
        <section id="g-top">
            <div class="g-container">
                <div class="g-grid">
                    <div class="g-block size-60">
                        <div class="g-content">
                            <div class="moduletable ">
                                <div class="g-particle">
                                    <nav class="g-main-nav" role="navigation" data-g-hover-expand="true">
                                        <ul class="g-toplevel">
                                            <li class="g-menu-item g-menu-item-type-url g-menu-item-128 g-standard  ">
                                            <a class="g-menu-item-container" href="#">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Advertise</span>
                                            </span>
                                            </a>
                                            </li>
                                            <li class="g-menu-item g-menu-item-type-url g-menu-item-129 g-standard  ">
                                                <a class="g-menu-item-container" href="#">
                                                    <span class="g-menu-item-content">
                                                        <span class="g-menu-item-title">Contact Us</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="g-menu-item g-menu-item-type-url g-menu-item-130 g-standard  ">
                                                <a class="g-menu-item-container" href="#">
                                                    <span class="g-menu-item-content">
                                                        <span class="g-menu-item-title">Write For Us</span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="g-menu-item g-menu-item-type-url g-menu-item-131 g-parent g-standard g-menu-item-link-parent ">
                                                <a class="g-menu-item-container" href="#">
                                                    <span class="g-menu-item-content">
                                                        <span class="g-menu-item-title">Dropdown</span>
                                                    </span>
                                                    <span class="g-menu-parent-indicator" data-g-menuparent=""></span>
                                                </a>
                                                <ul class="g-dropdown g-inactive g-fade g-dropdown-right">
                                                    <li class="g-dropdown-column">
                                                        <div class="g-grid">
                                                            <div class="g-block size-100">
                                                                <ul class="g-sublevel">
                                                                    <li class="g-level-1 g-go-back">
                                                                        <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                                                    </li>
                                                                    <li class="g-menu-item g-menu-item-type-url g-menu-item-132  ">
                                                                        <a class="g-menu-item-container" href="#">
                                                                            <span class="g-menu-item-content">
                                                                                <span class="g-menu-item-title">Subitem 1</span>
                                                                            </span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="g-menu-item g-menu-item-type-url g-menu-item-133  ">
                                                                        <a class="g-menu-item-container" href="#">
                                                                            <span class="g-menu-item-content">
                                                                                <span class="g-menu-item-title">Subitem 2</span>
                                                                            </span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="g-menu-item g-menu-item-type-url g-menu-item-134  ">
                                                                        <a class="g-menu-item-container" href="#">
                                                                            <span class="g-menu-item-content">
                                                                                <span class="g-menu-item-title">Subitem 3</span>
                                                                            </span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="g-block hidden-phone size-40">
                        <div class="g-content">
                            <div class="moduletable ">
                                <div class="g-particle">
                                    <div class="g-social" >
                                        <a target="_blank" href="http://facebook.com" data-uk-tooltip="{pos:'bottom'}" title="Facebook"   >
                                            <span class="fa fa-facebook fa-fw"></span>
                                        </a>
                                        <a target="_blank" href="http://twitter.com" data-uk-tooltip="{pos:'bottom'}" title="Twitter"   >
                                            <span class="fa fa-twitter fa-fw"></span>
                                        </a>
                                        <a target="_blank" href="https://plus.google.com/" data-uk-tooltip="{pos:'bottom'}" title="Google+"   >
                                            <span class="fa fa-google-plus fa-fw"></span>
                                        </a>
                                        <a target="_blank" href="http://linkedin.com" data-uk-tooltip="{pos:'bottom'}" title="Linkedin"   >
                                            <span class="fa fa-linkedin fa-fw"></span>
                                        </a>
                                        <a target="_blank" href="http://dribbble.com" data-uk-tooltip="{pos:'bottom'}" title="Dribbble"   >
                                            <span class="fa fa-dribbble fa-fw"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<header id="g-header">
    <div class="g-container">
        <div class="g-grid">
            <div class="g-block size-30">
                <div class="g-content g-particle">
                    <a href="/" title="Headlines" rel="home" class="g-logo ">
                        <img src="images/logo.png" alt="Headlines" width="170" height="70" /></a>
                </div>
            </div>
            <div class="g-block hidden-phone size-70">
                <div class="g-content">
                    <div class="platform-content">
                        <div class="moduletable ">
                            <div class="bannergroup">
                                <div class="banneritem">
                                <a href="/" target="_blank" title="Header Banner">
                                    <img src="images/banner1.png" alt="Header Banner" width="468" height="60" />
                                </a>
                                <div class="clr"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<section id="g-navigation" data-uk-sticky="">
    <div class="g-container">
        <div class="g-grid">
        <div class="g-block size-75">
            <div class="g-content g-particle">
                <nav class="g-main-nav" role="navigation" data-g-mobile-target data-g-hover-expand="true">
                    <ul class="g-toplevel">
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-101 g-parent active g-standard g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="/templates/headlines/index.php">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">Home</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade g-dropdown-right">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                        <div class="g-block size-100">
                                            <ul class="g-sublevel">
                                                <li class="g-level-1 g-go-back">
                                                    <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                                </li>
                                                <li class="g-menu-item g-menu-item-type-component g-menu-item-125  ">
                                                    <a class="g-menu-item-container" href="#">
                                                        <span class="g-menu-item-content">
                                                            <span class="g-menu-item-title">Home 1 (Default)</span>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="g-menu-item g-menu-item-type-component g-menu-item-126  ">
                                                    <a class="g-menu-item-container" href="#">
                                                        <span class="g-menu-item-content">
                                                            <span class="g-menu-item-title">Home 2</span>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="g-menu-item g-menu-item-type-component g-menu-item-236 active  ">
                                                    <a class="g-menu-item-container" href="###">
                                                        <span class="g-menu-item-content">
                                                            <span class="g-menu-item-title">Fullwidth Layout</span>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-229 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="/templates/headlines/index.php/business">
                                <span class="g-menu-item-content"><span class="g-menu-item-title">Business</span></span>
                                <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                        <div class="g-block size-100">
                                            <ul class="g-sublevel">
                                                <li class="g-level-1 g-go-back"><a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a></li>
                                                <li class="g-menu-item g-menu-item-type-particle g-menu-item-business---particle-0vCDc  ">
                                                    <div class="g-menu-item-container" data-g-menuparent="">
                                                        <div class="menu-item-particle">
                                                            <div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
                                                                <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
                                                                    <div class="uk-slider-container">
                                                                        <ul class="uk-slideset uk-grid">
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business g-featured-article">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="##">
                                                                                        <img src="images/business9.jpg" width="1000" height="500" alt="Vestibulum faucibus mollis tel massa id est dolor" />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="##">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
                                                                                            <div class="g-article-details details-">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#">
                                                                                            <img src="images/business8.jpg" width="1000" height="500" alt="Maecenas facilisis dapibus lectus in metus odio" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Maecenas facilisis dapibus lectus in metus odio</a></h4>
                                                                                        <div class="g-article-details details-"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business">
                                                                                    <div class="g-content-pro-image">
                                                                                    <a href="#">
                                                                                        <img src="images/business7.jpg" width="1000" height="500" alt="Donec pulvinar nisi vitae odio suscipit sit amet lectu" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Donec pulvinar nisi vitae odio suscip</a></h4>
                                                                                        <div class="g-article-details details-"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#">
                                                                                            <img src="images/business6.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Aliquam sodales quam in era neque porttitor</a></h4>
                                                                                        <div class="g-article-details details-"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#">
                                                                                        <img src="images/business5.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed</a></h4>
                                                                                        <div class="g-article-details details-">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-business">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#">
                                                                                        <img src="images/business4.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi</a></h4>
                                                                                        <div class="g-article-details details-">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                                                    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-230 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="#">
                                <span class="g-menu-item-content">
                                    <span class="g-menu-item-title">Lifestyle</span>
                                </span>
                                <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                        <div class="g-block size-100">
                                            <ul class="g-sublevel">
                                                <li class="g-level-1 g-go-back">
                                                    <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                                </li>
                                                <li class="g-menu-item g-menu-item-type-particle g-menu-item-lifestyle---particle-UsJxk  ">
                                                    <div class="g-menu-item-container" data-g-menuparent="">
                                                        <div class="menu-item-particle">
                                                            <div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
                                                                <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
                                                                    <div class="uk-slider-container">
                                                                        <ul class="uk-slideset uk-grid">
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle g-featured-article">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"><img src="images/lifestyle12.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed sit amet" /></a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed sit amet</a></h4>
                                                                                        <div class="g-article-details details-"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"><img src="images/lifestyle11.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi mauris orci" />
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi mauris orci</a></h4>
                                                                                        <div class="g-article-details details-"> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"> <img src="images/lifestyle10.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eget turpis" /> </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eget turpis</a></h4>
                                                                                        <div class="g-article-details details-"> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"> <img src="images/lifestyle9.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" /> </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
                                                                                        <div class="g-article-details details-"> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"> <img src="images/lifestyle8.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh mauris" /> </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh mauris</a></h4>
                                                                                        <div class="g-article-details details-"> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            <li>
                                                                                <div class="g-content-pro-item uk-overlay g-cat-lifestyle">
                                                                                    <div class="g-content-pro-image">
                                                                                        <a href="#"> <img src="images/lifestyle7.jpg" width="1000" height="500" alt="Lorem ipsum dolor sit amet, conse adipiscing" /> </a>
                                                                                    </div>
                                                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                                                        <h4 class="g-content-pro-title"><a href="#">Lorem ipsum dolor sit amet, conse adipiscing</a></h4>
                                                                                        <div class="g-article-details details-"> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                                                    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-231 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="#">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">Technology</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                    <div class="g-block size-100">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-particle g-menu-item-technology---particle-6i1qf  ">
                                            <div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
                                            <div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
                                            <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
                                            <div class="uk-slider-container">
                                            <ul class="uk-slideset uk-grid">
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology">
                                                        <div class="g-content-pro-image">
                                                        <a href="#">
                                                        <img src="images/technology9.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis sit amet" />
                                                        </a>
                                                        </div>
                                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                        <h4 class="g-content-pro-title"><a href="#">Phasellus eget augue est fring metus felis sit amet</a></h4>
                                                        <div class="g-article-details details-">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology g-featured-article">
                                                        <div class="g-content-pro-image">
                                                        <a href="#">
                                                        <img src="images/technology8.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh auctor" />
                                                        </a>
                                                        </div>
                                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                        <h4 class="g-content-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh auctor</a></h4>
                                                        <div class="g-article-details details-">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/technology/29-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus">
                                                    <img src="images/technology7.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor quis metus" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/technology/29-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus">Aliquam sodales quam in era neque porttitor quis metus</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology">
                                                    <div class="g-content-pro-image">
                                                    <a href="#">
                                                    <img src="images/technology6.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed posuere" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed posuere</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology">
                                                    <div class="g-content-pro-image">
                                                    <a href="#">
                                                    <img src="images/technology5.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi non mi enim" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi non mi enim</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-technology">
                                                        <div class="g-content-pro-image">
                                                        <a href="#">
                                                        <img src="images/technology4.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
                                                        </a>
                                                        </div>
                                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                        <h4 class="g-content-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
                                                        <div class="g-article-details details-">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            </div>
                                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-232 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="/templates/headlines/index.php/fashion">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">Fashion</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                    <div class="g-block size-100">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-particle g-menu-item-fashion---particle-1Ex12  ">
                                            <div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
                                            <div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
                                            <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
                                            <div class="uk-slider-container">
                                            <ul class="uk-slideset uk-grid">
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion">
                                                    <div class="g-content-pro-image">
                                                    <a href="#">
                                                    <img src="images/fashion9.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh mauris" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh mauris</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion">
                                                    <div class="g-content-pro-image">
                                                    <a href="#">
                                                    <img src="images/fashion8.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion g-featured-article">
                                                        <div class="g-content-pro-image">
                                                            <a href="#">
                                                            <img src="images/fashion7.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
                                                            </a>
                                                        </div>
                                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                        <h4 class="g-content-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
                                                        <div class="g-article-details details-">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion">
                                                        <div class="g-content-pro-image">
                                                            <a href="#">
                                                            <img src="images/fashion6.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi non mi enim" />
                                                            </a>
                                                        </div>
                                                        <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                        <h4 class="g-content-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi non mi enim</a></h4>
                                                        <div class="g-article-details details-">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/fashion/37-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">
                                                    <img src="images/fashion5.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed posuere" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/fashion/37-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">Proin volutpat tristique diam quis euismod sed posuere</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-fashion">
                                                    <div class="g-content-pro-image">
                                                    <a href="#">
                                                    <img src="images/fashion4.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor quis metus" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="#">Aliquam sodales quam in era neque porttitor quis metus</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            </div>
                                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-component g-menu-item-233 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="/templates/headlines/index.php/travel">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">Travel</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                    <div class="g-block size-100">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-particle g-menu-item-travel---particle-QRP9Y  ">
                                            <div class="g-menu-item-container" data-g-menuparent=""> <div class="menu-item-particle">
                                            <div class="g-content-pro-slideset style3 uk-text-left gutter-enabled" >
                                            <div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 3, large: 3, duration: 200,  animation: 'fade'}">
                                            <div class="uk-slider-container">
                                            <ul class="uk-slideset uk-grid">
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-travel">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/travel/46-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">
                                                    <img src="images/travel9.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed posuere" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/46-proin-volutpat-tristique-diam-quis-euismod-sed-posuere-ut">Proin volutpat tristique diam quis euismod sed posuere</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-travel">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/travel/45-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis">
                                                    <img src="images/travel8.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi non mi enim" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/45-class-aptent-taciti-socio-ad-litora-est-fringi-non-mi-enim-quis">Class aptent taciti socio ad litora est fringi non mi enim</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-travel">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/travel/44-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan">
                                                    <img src="images/travel7.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/44-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-eros-accumsan">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-travel">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/travel/43-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor">
                                                    <img src="images/travel6.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/43-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="g-content-pro-item uk-overlay g-cat-travel g-featured-article">
                                                    <div class="g-content-pro-image">
                                                    <a href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">
                                                    <img src="images/travel5.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultri tem nabh mauris orci" />
                                                    </a>
                                                    </div>
                                                    <div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom">
                                                    <h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">Etiam eu sapien at purus ultri tem nabh mauris orci</a></h4>
                                                    <div class="g-article-details details-">
                                                    </div>
                                                    </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            </div>
                                            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-url g-menu-item-150 g-parent g-fullwidth g-menu-item-link-parent ">
                            <a class="g-menu-item-container" href="#">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">More</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade ">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                    <div class="g-block size-25">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-151  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/contact-us">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Contact Us</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-152  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/about-us">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">About Us</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-153  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/roksprocket-mosaic-default">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">RokSprocket Mosaic (Default)</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-154  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/roksprocket-mosaic-gallery">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">RokSprocket Mosaic (Gallery)</span>
                                            </span>
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                    <div class="g-block size-25">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-168  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/search-results">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Search Results</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-176  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/member-login">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Member Login</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-178  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/registration-form">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Registration Form</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-181  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/pagessss">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">404 Page</span>
                                            </span>
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                    <div class="g-block size-25">
                                        <ul class="g-sublevel">
                                            <li class="g-level-1 g-go-back">
                                                <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                            </li>
                                            <li class="g-menu-item g-menu-item-type-component g-menu-item-192  ">
                                                <a class="g-menu-item-container" href="/templates/headlines/index.php/more/module-positions">
                                                <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">Layout Manager</span>
                                                </span>
                                                </a>
                                            </li>
                                            <li class="g-menu-item g-menu-item-type-component g-menu-item-193  ">
                                                <a class="g-menu-item-container" href="/templates/headlines/index.php/more/menu-editor">
                                                <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">Menu Editor</span>
                                                </span>
                                                </a>
                                            </li>
                                            <li class="g-menu-item g-menu-item-type-component g-menu-item-194  ">
                                                <a class="g-menu-item-container" href="/templates/headlines/index.php/more/particles">
                                                <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">Particles</span>
                                                </span>
                                                </a>
                                            </li>
                                                <li class="g-menu-item g-menu-item-type-component g-menu-item-197  ">
                                                <a class="g-menu-item-container" href="/templates/headlines/index.php/more/uikit-framework">
                                                <span class="g-menu-item-content">
                                                <span class="g-menu-item-title">UIkit Framework</span>
                                                </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="g-block size-25">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-198  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/scrollreveal-js">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">ScrollReveal.js</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-191  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/module-variations">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Module Variations</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-component g-menu-item-196  ">
                                            <a class="g-menu-item-container" href="/templates/headlines/index.php/more/typography">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Typography</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-199  ">
                                            <a class="g-menu-item-container" href="###" target="_blank">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Documentation</span>
                                            </span>
                                            </a>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="g-menu-item g-menu-item-type-url g-menu-item-184 g-parent g-standard g-menu-item-link-parent presets-demo">
                            <a class="g-menu-item-container" href="#">
                            <span class="g-menu-item-content">
                            <span class="g-menu-item-title">Presets</span>
                            </span>
                            <span class="g-menu-parent-indicator" data-g-menuparent=""></span> </a>
                            <ul class="g-dropdown g-inactive g-fade g-dropdown-right">
                                <li class="g-dropdown-column">
                                    <div class="g-grid">
                                    <div class="g-block size-100">
                                    <ul class="g-sublevel">
                                        <li class="g-level-1 g-go-back">
                                            <a class="g-menu-item-container" href="#" data-g-menuparent=""><span>Back</span></a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-185  preset1">
                                            <a class="g-menu-item-container" href="/templates/headlines/?presets=preset1">
                                            <span class="g-menu-item-content">
                                            <span class="g-menu-item-title">Preset 1 (Default)</span>
                                            </span>
                                            </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-186  preset2">
                                        <a class="g-menu-item-container" href="/templates/headlines/?presets=preset2">
                                        <span class="g-menu-item-content">
                                        <span class="g-menu-item-title">Preset 2</span>
                                        </span>
                                        </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-187  preset3">
                                        <a class="g-menu-item-container" href="/templates/headlines/?presets=preset3">
                                        <span class="g-menu-item-content">
                                        <span class="g-menu-item-title">Preset 3</span>
                                        </span>
                                        </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-188  preset4">
                                        <a class="g-menu-item-container" href="/templates/headlines/?presets=preset4">
                                        <span class="g-menu-item-content">
                                        <span class="g-menu-item-title">Preset 4</span>
                                        </span>
                                        </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-189  preset5">
                                        <a class="g-menu-item-container" href="/templates/headlines/?presets=preset5">
                                        <span class="g-menu-item-content">
                                        <span class="g-menu-item-title">Preset 5</span>
                                        </span>
                                        </a>
                                        </li>
                                        <li class="g-menu-item g-menu-item-type-url g-menu-item-190  preset6">
                                        <a class="g-menu-item-container" href="/templates/headlines/?presets=preset6">
                                        <span class="g-menu-item-content">
                                        <span class="g-menu-item-title">Preset 6</span>
                                        </span>
                                        </a>
                                        </li>
                                    </ul>
                                    </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="g-block size-25">
        <div class="g-content">
        <div class="moduletable ">
        <div class="g-particle">
        <div class="g-search-login" >
        <div class="g-buttons">
            <div class="g-search-button">
            <a id="header-search-toggle" href="#" data-uk-tooltip title="Search">
            <i class="fa fa-search"></i>
            </a>
            </div>
            <div class="g-login-button">
            <a href="#modal-login" data-uk-modal="{center:true}" data-uk-tooltip title="Login">
            <i class="fa fa-user"></i>
            </a>
            </div>
            <div class="g-offcanvas-button">
            <a class="offcanvas-toggle-particle" data-offcanvas-toggle="" data-uk-tooltip title="Offcanvas">
            <i class="fa fa-bars"></i>
            </a>
            </div>
        </div>
        <div id="header-search">
            <div class="g-block">
            <div class="g-content">
            <a class="uk-close"></a>
            <div class="moduletable">
            <div class="search mod_search93">
            <form action="/templates/headlines/index.php" method="post" class="form-inline">
            <label for="mod-search-searchword" class="element-invisible">Search ...</label> <input autocomplete="off" name="searchword" id="mod-search-searchword" maxlength="200"  class="inputbox search-query" type="search" size="20" placeholder="Search ..." />   <input type="hidden" name="task" value="search" />
            <input type="hidden" name="option" value="com_search" />
            <input type="hidden" name="Itemid" value="101" />
            </form>
            </div>
            </div>
            </div>
            </div>
        </div>
        <div id="modal-login" class="uk-modal">
        <a class="uk-modal-close uk-close"></a>
        <div class="uk-modal-dialog">
            <div class="moduletabletitle-border">
                <h3>Login To Your Account</h3>
                <form action="##" method="post" id="login-form" class="form-inline">
                    <div class="userdata">
                    <div id="form-login-username" class="control-group">
                    <div class="controls">
                    <label for="modlgn-username" class="element-invisible">Username</label>
                    <input id="modlgn-username" type="text" name="username" tabindex="0" size="18" placeholder="Username" />
                    </div>
                    </div>
                    <div id="form-login-password" class="control-group">
                        <div class="controls">
                        <label for="modlgn-passwd" class="element-invisible">Password</label>
                        <input id="modlgn-passwd" type="password" name="password" tabindex="0" size="18" placeholder="Password" />
                        </div>
                    </div>
                    <div id="form-login-remember" class="control-group checkbox">
                    <label for="modlgn-remember" class="control-label">Remember Me</label> <input id="modlgn-remember" type="checkbox" name="remember" class="inputbox" value="yes"/>
                    </div>
                    <div id="form-login-submit" class="control-group">
                    <div class="controls">
                    <button type="submit" tabindex="0" name="Submit" class="btn btn-primary">Log in</button>
                    </div>
                    </div>
                    <ul class="unstyled">
                        <li>
                        <a href="/templates/headlines/index.php/more/registration-form">
                        <i class="fa fa-question-circle"></i>Create an account</a>
                        </li>
                        <li>
                        <a href="/templates/headlines/index.php/more/registration-form?view=remind">
                        <i class="fa fa-question-circle"></i>Forgot your username?</a>
                        </li>
                        <li>
                        <a href="/templates/headlines/index.php/more/registration-form?view=reset">
                        <i class="fa fa-question-circle"></i>Forgot your password?</a>
                        </li>
                    </ul>
                    <input type="hidden" name="option" value="com_users" />
                    <input type="hidden" name="task" value="user.login" />
                    <input type="hidden" name="return" value="aHR0cDovL2RlbW8uaW5zcGlyZXRoZW1lLmNvbS90ZW1wbGF0ZXMvaGVhZGxpbmVzLw==" />
                    <input type="hidden" name="0d14b02b3838edaccae7d7d9c3c6cf86" value="1" />   </div>
                </form>
            </div>
        </div>
        </div>
        </div>
        </div>  </div>
        </div>
        </div>
        </div>
    </div>
</section>
<section id="g-showcase">
    <div class="g-container">
    <div class="g-grid">
    <div         
             class="g-block size-100">
    <div class="g-content">
    <div class="moduletable ">
    <div class="g-particle">
    <div class="g-top-news style1 g-inline_1209834220 gutter-disabled" >
    <div class="g-top-news-container clearfix">
    <div class="g-top-news-item g-top-news-main g-tn1" style="height: 450px;">
    <div class="g-top-news-item-inner" style="background-image: url(images/business9.jpg);">
    <div class="g-top-news-item-image">
    <a href="##"></a>
    </div>
    <div class="g-top-news-item-info">
    <span class="g-article-category">
    <a href="/templates/headlines/index.php/business">
    <span class="g-cat-business">Business</span>
    </a>
    </span>
    <h4 class="g-article-title">
    <a href="##">Vestibulum faucibus mollis tel massa id est dolor</a>
    </h4>
    <div class="g-article-details">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>15 September</span>
    </div>
    <div class="g-article-text">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi...</div>
    </div>
    </div>
    </div>
    <div class="g-top-news-item g-top-news-secondary g-tn2" style="height: 225px;">
    <div class="g-top-news-item-inner" style="background-image: url(images/lifestyle12.jpg);">
    <div class="g-top-news-item-image">
    <a href="#"></a>
    </div>
    <div class="g-top-news-item-info">
    <span class="g-article-category">
    <a href="#">
    <span class="g-cat-lifestyle">Lifestyle</span>
    </a>
    </span>
    <h4 class="g-article-title">
    <a href="#">Proin volutpat tristique diam quis euismod sed sit...</a>
    </h4>
    <div class="g-article-details">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>14 September</span>
    </div>
    <div class="g-article-text">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi...</div>
    </div>
    </div>
    </div>
    <div class="g-top-news-item g-top-news-secondary g-tn3" style="height: 225px;">
    <div class="g-top-news-item-inner" style="background-image: url(images/fashion7.jpg);">
    <div class="g-top-news-item-image">
    <a href="#"></a>
    </div>
    <div class="g-top-news-item-info">
    <span class="g-article-category">
    <a href="/templates/headlines/index.php/fashion">
    <span class="g-cat-fashion">Fashion</span>
    </a>
    </span>
    <h4 class="g-article-title">
    <a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eros</a>
    </h4>
    <div class="g-article-details">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>10 September</span>
    </div>
    <div class="g-article-text">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi...</div>
    </div>
    </div>
    </div>
    <div class="g-top-news-item g-top-news-secondary g-tn4" style="height: 225px;">
    <div class="g-top-news-item-inner" style="background-image: url(images/technology8.jpg);">
    <div class="g-top-news-item-image">
    <a href="#"></a>
    </div>
    <div class="g-top-news-item-info">
    <span class="g-article-category">
    <a href="#">
    <span class="g-cat-technology">Technology</span>
    </a>
    </span>
    <h4 class="g-article-title">
    <a href="#">Etiam eu sapien at purus ultricies tempor nabh auctor</a>
    </h4>
    <div class="g-article-details">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>10 September</span>
    </div>
    <div class="g-article-text">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi...</div>
    </div>
    </div>
    </div>
    <div class="g-top-news-item g-top-news-secondary g-tn5" style="height: 225px;">
    <div class="g-top-news-item-inner" style="background-image: url(images/travel5.jpg);">
    <div class="g-top-news-item-image">
    <a href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci"></a>
    </div>
    <div class="g-top-news-item-info">
    <span class="g-article-category">
    <a href="/templates/headlines/index.php/travel">
    <span class="g-cat-travel">Travel</span>
    </a>
    </span>
    <h4 class="g-article-title">
    <a href="/templates/headlines/index.php/travel/42-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci">Etiam eu sapien at purus ultri tem nabh mauris orci</a>
    </h4>
    <div class="g-article-details">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>05 September</span>
    </div>
    <div class="g-article-text">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi...</div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>  </div>
    </div>
    </div>
    </div>
    </div>
</section>
<section id="g-system-messages">
    <div class="g-container">
    <div          
             class="g-grid">
    <div         
             class="g-block size-100">
    <div class="g-system-messages">
    </div>
    </div>
    </div>
    </div>
</section>
<section id="g-container-main" class="g-wrapper">
    <div class="g-container">
    <div          
             class="g-grid">
    <div         
             class="g-block size-67">
    <section id="g-mainbody">
        <div          
                 class="g-grid">
        <div         
                 class="g-block size-100">
        <div class="g-content">
        <div class="moduletable ">
        <h3 class="g-title"><span>Business</span></h3>
        <div class="g-particle">
        <div class="g-news-pro" >
        <div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
        <div class="uk-slider-container">
        <div class="uk-slideset uk-grid">
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-3">
        <div class="g-news-pro-item vertical g-cat-business g-featured-article">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/business9.jpg);  height: 150px;">
        <a href="##"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="##">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 15</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-business">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/business8.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Maecenas facilisis dapibus lectus in metus odio</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-business">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/business7.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Donec pulvinar nisi vitae odio suscipit sit amet...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 11</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-3">
        <div class="g-news-pro-item vertical g-cat-business">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/business6.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Aliquam sodales quam in era neque porttitor</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 10</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-business">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/business5.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 09</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-business">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/business4.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 08</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-3">
        <div class="g-news-pro-item vertical g-cat-business">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/business3.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/business/4-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/business/4-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum">Uisque ac gravida ligula nunc nisi risus ipsum</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 07</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-business">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/business2.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/business/3-phasellus-eget-augue-est-fring-metus-felis"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/business/3-phasellus-eget-augue-est-fring-metus-felis">Phasellus eget augue est fring metus felis</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 06</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-business">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/business1.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/business/2-etiam-eu-sapien-at-purus-ultricies-tempor-nabh"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/business/2-etiam-eu-sapien-at-purus-ultricies-tempor-nabh">Etiam eu sapien at purus ultricies tempor nabh</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 05</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-nav">
        <div class="g-news-pro-arrows">
        <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
        <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
        </div>
        <ul class="uk-slideset-nav uk-dotnav uk-flex-center">
        <li data-uk-slideset-item="0"><a href="#"></a></li>
        <li data-uk-slideset-item="1"><a href="#"></a></li>
        <li data-uk-slideset-item="2"><a href="#"></a></li>
        <li data-uk-slideset-item="3"><a href="#"></a></li>
        <li data-uk-slideset-item="4"><a href="#"></a></li>
        <li data-uk-slideset-item="5"><a href="#"></a></li>
        <li data-uk-slideset-item="6"><a href="#"></a></li>
        <li data-uk-slideset-item="7"><a href="#"></a></li>
        <li data-uk-slideset-item="8"><a href="#"></a></li>
        </ul>
        </div>
        </div>
        </div>
        </div>  </div>
        </div>
        </div>
        </div>
        <div          
                 class="g-grid">
        <div         
                 class="g-block size-100">
        <div class="g-content">
        <div class="moduletable ">
        <h3 class="g-title"><span>Lifestyle</span></h3>
        <div class="g-particle">
        <div class="g-news-pro" >
        <div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
        <div class="uk-slider-container">
        <div class="uk-slideset uk-grid">
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle g-featured-article">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle12.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed sit amet</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 14</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>160</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle11.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi mauris orci</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 13</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>65</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle10.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eget turpis</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>56</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle9.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 11</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>54</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle8.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh mauris</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 10</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>37</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle7.jpg); width: 260px; height: 130px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Lorem ipsum dolor sit amet, conse adipiscing</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 09</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>39</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle6.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/16-etiam-eu-sapien-at-purus-ultricies-tempor-nabh"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/16-etiam-eu-sapien-at-purus-ultricies-tempor-nabh">Etiam eu sapien at purus ultricies tempor nabh</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 08</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>30</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle5.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/15-class-aptent-taciti-socio-ad-litora-est-fringi"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/15-class-aptent-taciti-socio-ad-litora-est-fringi">Class aptent taciti socio ad litora est fringi</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 07</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>34</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle4.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/14-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/14-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum">Uisque ac gravida ligula nunc nisi risus ipsum</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 06</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>37</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle3.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/13-phasellus-eget-augue-est-fring-metus-felis"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/13-phasellus-eget-augue-est-fring-metus-felis">Phasellus eget augue est fring metus felis</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 05</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>53</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle2.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/12-proin-volutpat-tristique-diam-quis-euismod-sed"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/12-proin-volutpat-tristique-diam-quis-euismod-sed">Proin volutpat tristique diam quis euismod sed</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 04</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>42</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle1.jpg); width: 260px; height: 130px;">
        <a href="/templates/headlines/index.php/lifestyle/11-aliquam-sodales-quam-in-era-neque-porttitor"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/11-aliquam-sodales-quam-in-era-neque-porttitor">Aliquam sodales quam in era neque porttitor</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 03</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        <span class="g-article-hits">
        <i class="fa fa-eye"></i>48</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. Etiam...</div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-nav">
        <div class="g-news-pro-arrows">
        <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
        <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
        </div>
        <ul class="uk-slideset-nav uk-dotnav uk-flex-center">
            <li data-uk-slideset-item="0"><a href="#"></a></li>
            <li data-uk-slideset-item="1"><a href="#"></a></li>
            <li data-uk-slideset-item="2"><a href="#"></a></li>
            <li data-uk-slideset-item="3"><a href="#"></a></li>
            <li data-uk-slideset-item="4"><a href="#"></a></li>
            <li data-uk-slideset-item="5"><a href="#"></a></li>
            <li data-uk-slideset-item="6"><a href="#"></a></li>
            <li data-uk-slideset-item="7"><a href="#"></a></li>
            <li data-uk-slideset-item="8"><a href="#"></a></li>
            <li data-uk-slideset-item="9"><a href="#"></a></li>
            <li data-uk-slideset-item="10"><a href="#"></a></li>
            <li data-uk-slideset-item="11"><a href="#"></a></li>
        </ul>
        </div>
        </div>
        </div>
        </div>  </div>
        </div>
        </div>
        </div>
        <div          
                 class="g-grid">
        <div         
                 class="g-block size-100">
        <div class="g-content">
        <div class="moduletable ">
        <h3 class="g-title"><span>Technology</span></h3>
        <div class="g-particle">
        <div class="g-news-pro" >
        <div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
        <div class="uk-slider-container">
        <div class="uk-slideset uk-grid">
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-3">
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology9.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Phasellus eget augue est fring metus felis sit...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 11</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology g-featured-article">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology8.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 10</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology7.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/technology/29-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/technology/29-aliquam-sodales-quam-in-era-neque-porttitor-quis-metus">Aliquam sodales quam in era neque porttitor quis...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 09</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-3">
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology6.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 08</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology5.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 07</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology4.jpg);  height: 150px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 07</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-page">
        <div class="uk-grid uk-grid-width-1-3">
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology3.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/technology/25-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor-2"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/technology/25-phasellus-eget-augue-est-fring-metus-felis-ipsum-dolor-2">Phasellus eget augue est fring metus felis ipsum...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 06</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology2.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/technology/24-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci-2"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/technology/24-etiam-eu-sapien-at-purus-ultricies-tempor-nabh-mauris-orci-2">Etiam eu sapien at purus ultricies temor nabh...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 05</span>
        </div>
        <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        <div class="g-news-pro-item vertical g-cat-technology">
        <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology1.jpg);  height: 150px;">
        <a href="/templates/headlines/index.php/technology/23-aliquam-sodales-quam-in-era-neque-porttitor-lorem-ipsum"></a>
        </div>
        <div class="g-info-container">
            <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/technology/23-aliquam-sodales-quam-in-era-neque-porttitor-lorem-ipsum">Aliquam sodales quam in era neque porttitor lorem</a></h4>
            <div class="g-article-details details-show">
            <span class="g-article-date"><i class="fa fa-clock-o"></i>September 04</span>
            </div>
            <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <div class="g-news-pro-nav">
        <div class="g-news-pro-arrows">
            <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
            <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
        </div>
        <ul class="uk-slideset-nav uk-dotnav uk-flex-center">
            <li data-uk-slideset-item="0"><a href="#"></a></li>
            <li data-uk-slideset-item="1"><a href="#"></a></li>
            <li data-uk-slideset-item="2"><a href="#"></a></li>
            <li data-uk-slideset-item="3"><a href="#"></a></li>
            <li data-uk-slideset-item="4"><a href="#"></a></li>
            <li data-uk-slideset-item="5"><a href="#"></a></li>
            <li data-uk-slideset-item="6"><a href="#"></a></li>
            <li data-uk-slideset-item="7"><a href="#"></a></li>
            <li data-uk-slideset-item="8"><a href="#"></a></li>
        </ul>
        </div>
        </div>
        </div>
        </div>  </div>
        </div>
        </div>
        </div>
    </section>
    </div>
    <div         
             class="g-block size-33">
    <aside id="g-aside">
    <div          
             class="g-grid">
    <div         
             class="g-block size-100">
    <div class="g-content">
    <div class="moduletable box2">
    <h3 class="g-title"><span>Most Popular</span></h3>
    <div class="g-particle">
    <div class="g-content-pro style1 uk-text-left gutter-enabled" >
    <div class="g-grid">
    <div class="g-block">
    <div class="g-content">
    <div class="g-content-pro-item g-cat-business g-featured-article">
    <div class="g-content-pro-image">
    <a href="##">
    <img src="images/business9.jpg" width="1000" height="500" alt="Vestibulum faucibus mollis tel massa id est dolor" />
    </a>
    </div>
    <div class="g-info-container">
    <h4 class="g-content-pro-title"><a href="##">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>September 15</span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>326</span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>  <div class="g-grid">
    <div class="g-block">
    <div class="g-content">
    <div class="g-content-pro-item g-cat-lifestyle g-featured-article">
    <div class="g-content-pro-image">
    <a href="#">
    <img src="images/lifestyle12.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed sit amet" />
    </a>
    </div>
    <div class="g-info-container">
    <h4 class="g-content-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed sit amet</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>September 14</span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>160</span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>  </div><div class="moduletable box4">
    <h3 class="g-title"><span>Editors Choice</span></h3>
    <div class="g-particle">
    <div class="g-news-pro" >
    <div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
    <div class="uk-slider-container">
    <div class="uk-slideset uk-grid">
    <div class="g-news-pro-page">
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item vertical g-cat-lifestyle">
    <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/lifestyle1.jpg);  height: 150px;">
    <a href="/templates/headlines/index.php/lifestyle/11-aliquam-sodales-quam-in-era-neque-porttitor"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/11-aliquam-sodales-quam-in-era-neque-porttitor">Aliquam sodales quam in era neque...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>September 03</span>
    </div>
    <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
    </div>
    </div>
    </div>
    </div>
    <div class="g-news-pro-page">
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item vertical g-cat-lifestyle">
    <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/lifestyle2.jpg);  height: 150px;">
    <a href="/templates/headlines/index.php/lifestyle/12-proin-volutpat-tristique-diam-quis-euismod-sed"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/lifestyle/12-proin-volutpat-tristique-diam-quis-euismod-sed">Proin volutpat tristique diam quis...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>September 04</span>
    </div>
    <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
    </div>
    </div>
    </div>
    </div>
    <div class="g-news-pro-page">
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item vertical g-cat-technology">
    <div class="g-news-pro-image image-fullwidth image-link" style="background-image: url(images/technology1.jpg);  height: 150px;">
    <a href="/templates/headlines/index.php/technology/23-aliquam-sodales-quam-in-era-neque-porttitor-lorem-ipsum"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="/templates/headlines/index.php/technology/23-aliquam-sodales-quam-in-era-neque-porttitor-lorem-ipsum">Aliquam sodales quam in era neque...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>September 04</span>
    </div>
    <div class="g-news-pro-desc">Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at...</div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="g-news-pro-nav">
    <div class="g-news-pro-arrows">
    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
    </div>
    <ul class="uk-slideset-nav uk-dotnav uk-flex-center">
        <li data-uk-slideset-item="0"><a href="#"></a></li>
        <li data-uk-slideset-item="1"><a href="#"></a></li>
        <li data-uk-slideset-item="2"><a href="#"></a></li>
        <li data-uk-slideset-item="3"><a href="#"></a></li>
        <li data-uk-slideset-item="4"><a href="#"></a></li>
        <li data-uk-slideset-item="5"><a href="#"></a></li>
        <li data-uk-slideset-item="6"><a href="#"></a></li>
        <li data-uk-slideset-item="7"><a href="#"></a></li>
        <li data-uk-slideset-item="8"><a href="#"></a></li>
        <li data-uk-slideset-item="9"><a href="#"></a></li>
        <li data-uk-slideset-item="10"><a href="#"></a></li>
        <li data-uk-slideset-item="11"><a href="#"></a></li>
        <li data-uk-slideset-item="12"><a href="#"></a></li>
        <li data-uk-slideset-item="13"><a href="#"></a></li>
        <li data-uk-slideset-item="14"><a href="#"></a></li>
        <li data-uk-slideset-item="15"><a href="#"></a></li>
        <li data-uk-slideset-item="16"><a href="#"></a></li>
        <li data-uk-slideset-item="17"><a href="#"></a></li>
        <li data-uk-slideset-item="18"><a href="#"></a></li>
        <li data-uk-slideset-item="19"><a href="#"></a></li>
        <li data-uk-slideset-item="20"><a href="#"></a></li>
        <li data-uk-slideset-item="21"><a href="#"></a></li>
        <li data-uk-slideset-item="22"><a href="#"></a></li>
        <li data-uk-slideset-item="23"><a href="#"></a></li>
        <li data-uk-slideset-item="24"><a href="#"></a></li>
        <li data-uk-slideset-item="25"><a href="#"></a></li>
        <li data-uk-slideset-item="26"><a href="#"></a></li>
        <li data-uk-slideset-item="27"><a href="#"></a></li>
        <li data-uk-slideset-item="28"><a href="#"></a></li>
        <li data-uk-slideset-item="29"><a href="#"></a></li>
        <li data-uk-slideset-item="30"><a href="#"></a></li>
        <li data-uk-slideset-item="31"><a href="#"></a></li>
        <li data-uk-slideset-item="32"><a href="#"></a></li>
        <li data-uk-slideset-item="33"><a href="#"></a></li>
        <li data-uk-slideset-item="34"><a href="#"></a></li>
        <li data-uk-slideset-item="35"><a href="#"></a></li>
        <li data-uk-slideset-item="36"><a href="#"></a></li>
        <li data-uk-slideset-item="37"><a href="#"></a></li>
        <li data-uk-slideset-item="38"><a href="#"></a></li>
        <li data-uk-slideset-item="39"><a href="#"></a></li>
        <li data-uk-slideset-item="40"><a href="#"></a></li>
        <li data-uk-slideset-item="41"><a href="#"></a></li>
        <li data-uk-slideset-item="42"><a href="#"></a></li>
        <li data-uk-slideset-item="43"><a href="#"></a></li>
    </ul>
    </div>
    </div>
    </div>
    </div>  </div><div class="moduletable box2">
    <h3 class="g-title"><span>Recent News</span></h3>
    <div class="g-particle">
    <div class="g-news-pro" >
    <div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
    <div class="uk-slider-container">
    <div class="uk-slideset uk-grid">
    <div class="g-news-pro-page">
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-business g-featured-article">
    <div class="g-news-pro-image image-link" style="background-image: url(images/business9.jpg); width: 120px; height: 75px;">
    <a href="##"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="##">Vestibulum faucibus mollis tel massa...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Business                                   </span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>326</span>
    </div>
    </div>
    </div>
    </div>
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-lifestyle g-featured-article">
    <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle12.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Proin volutpat tristique diam quis...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>160</span>
    </div>
    </div>
    </div>
    </div>
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-lifestyle">
    <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle11.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Class aptent taciti socio ad litora...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>65</span>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="g-news-pro-page">
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-fashion">
    <div class="g-news-pro-image image-link" style="background-image: url(images/fashion9.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Etiam eu sapien at purus ultricies...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Fashion                                    </span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>53</span>
    </div>
    </div>
    </div>
    </div>
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-lifestyle">
    <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle10.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>56</span>
    </div>
    </div>
    </div>
    </div>
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-business">
    <div class="g-news-pro-image image-link" style="background-image: url(images/business8.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Maecenas facilisis dapibus lectus in...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Business                                   </span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>83</span>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="g-news-pro-page">
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-fashion">
    <div class="g-news-pro-image image-link" style="background-image: url(images/fashion8.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Phasellus eget augue est fring metus...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Fashion                                    </span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>64</span>
    </div>
    </div>
    </div>
    </div>
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-technology">
    <div class="g-news-pro-image image-link" style="background-image: url(images/technology9.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Phasellus eget augue est fring metus...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Technology                                 </span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>76</span>
    </div>
    </div>
    </div>
    </div>
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-lifestyle">
    <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle9.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Phasellus eget augue est fring metus...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
    <span class="g-article-hits">
    <i class="fa fa-eye"></i>54</span>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="g-news-pro-nav">
    <div class="g-news-pro-arrows">
    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
    </div>
    </div>
    </div>
    </div>
    </div>  </div>
    </div>
    </div>
    </div>
    </aside>
    </div>
    </div>
    </div>
</section>
<section id="g-mainbottom" class="pull-up">
<div class="g-container">
<div          
         class="g-grid">
<div         
         class="g-block size-100">
<div class="g-content">
<div class="moduletable ">
<h3 class="g-title"><span>News Gallery</span></h3>
<div class="g-particle">
<div class="g-content-pro-slideset style2 uk-text-left gutter-enabled" >
<div class="uk-slidenav-position" data-uk-slideset="{small: 1, medium: 4, large: 4, duration: 200,  animation: 'fade'}">
<div class="uk-slider-container">
<ul class="uk-slideset uk-grid">
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-business g-featured-article">
<div class="g-content-pro-image">
<a href="##">
<img src="images/business9.jpg" width="1000" height="500" alt="Vestibulum faucibus mollis tel massa id est dolor" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="##">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-lifestyle g-featured-article">
<div class="g-content-pro-image">
<a href="#">
<img src="images/lifestyle12.jpg" width="1000" height="500" alt="Proin volutpat tristique diam quis euismod sed sit amet" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed sit amet</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-business">
<div class="g-content-pro-image">
<a href="#">
<img src="images/business6.jpg" width="1000" height="500" alt="Aliquam sodales quam in era neque porttitor" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="#">Aliquam sodales quam in era neque porttitor</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-fashion">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/fashion/32-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-mollis-felis">
<img src="images/fashion1.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum mollis felis" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/fashion/32-uisque-ac-gravida-ligula-nunc-nisi-risus-ipsum-mollis-felis">Uisque ac gravida ligula nunc nisi risus ipsum mollis felis</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-business">
<div class="g-content-pro-image">
<a href="#">
<img src="images/business8.jpg" width="1000" height="500" alt="Maecenas facilisis dapibus lectus in metus odio" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="#">Maecenas facilisis dapibus lectus in metus odio</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-technology g-featured-article">
<div class="g-content-pro-image">
<a href="#">
<img src="images/technology8.jpg" width="1000" height="500" alt="Etiam eu sapien at purus ultricies tempor nabh auctor" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh auctor</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-business">
<div class="g-content-pro-image">
<a href="#">
<img src="images/business7.jpg" width="1000" height="500" alt="Donec pulvinar nisi vitae odio suscipit sit amet lectu" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="#">Donec pulvinar nisi vitae odio suscipit sit amet lectu</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-technology">
<div class="g-content-pro-image">
<a href="#">
<img src="images/technology9.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis sit amet" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="#">Phasellus eget augue est fring metus felis sit amet</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-fashion g-featured-article">
<div class="g-content-pro-image">
<a href="#">
<img src="images/fashion7.jpg" width="1000" height="500" alt="Uisque ac gravida ligula nunc nisi risus ipsum eros" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum eros</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-business">
<div class="g-content-pro-image">
<a href="/templates/headlines/index.php/business/2-etiam-eu-sapien-at-purus-ultricies-tempor-nabh">
<img src="images/business1.jpg" width="1000" height="499" alt=" Etiam eu sapien at purus ultricies tempor nabh" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="/templates/headlines/index.php/business/2-etiam-eu-sapien-at-purus-ultricies-tempor-nabh"> Etiam eu sapien at purus ultricies tempor nabh</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-lifestyle">
<div class="g-content-pro-image">
<a href="#">
<img src="images/lifestyle11.jpg" width="1000" height="500" alt="Class aptent taciti socio ad litora est fringi mauris orci" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi mauris orci</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
<li>
<div class="g-content-pro-item uk-overlay uk-overlay-hover g-cat-fashion">
<div class="g-content-pro-image">
<a href="#">
<img src="images/fashion8.jpg" width="1000" height="500" alt="Phasellus eget augue est fring metus felis ipsum dolor" />
</a>
</div>
<div class="g-info-container-style2 uk-overlay-panel uk-overlay-background uk-overlay-bottom uk-overlay-slide-bottom">
<h4 class="g-content-pro-title"><a href="#">Phasellus eget augue est fring metus felis ipsum dolor</a></h4>
<div class="g-article-details details-show">
</div>
</div>
</div>
</li>
</ul>
</div>
<a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
<a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
</div>
</div>
</div>  </div>
</div>
</div>
</div>
</div>
</section>
<footer id="g-footer">
<div class="g-container">
<div          
         class="g-grid">
<div         
         class="g-block size-33">
    <div class="g-content">
    <div class="moduletable ">
    <h3 class="g-title"><span>About Us</span></h3>
    <div class="g-particle">
                                        Lorem ipsum dolor sit amet, conse adipiscing elit. Maecenas mauris orci, pellentesque at vestibulum quis, porttitor eget turpis. Morbi porta orci et augue sollicitudin cursus ut eget ligula. 
                
        </div>  </div><div class="moduletable ">
    <div class="g-particle">
    <div class="g-contacts uk-margin-top" >
    <div class="g-contacts-item" >
    <span class="g-contacts-icon fa fa-home"></span>
    <span class="g-contact-value">64184 Vincent Place</span>
    </div>
    <div class="g-contacts-item" >
    <span class="g-contacts-icon fa fa-phone"></span>
    <span class="g-contact-value">0044 889 555 432</span>
    </div>
    <div class="g-contacts-item" >
    <span class="g-contacts-icon fa fa-envelope-o"></span>
    <span class="g-contact-value">office@headlines.com</span>
    </div>
    </div>
    </div>  </div>
    </div>
</div>
<div class="g-block size-34">
<div class="g-content">
<div class="moduletable ">
<h3 class="g-title"><span>Recent News</span></h3>
<div class="g-particle">
<div class="g-news-pro" >
<div class="uk-slidenav-position" data-uk-slideset="{duration: 200,  animation: 'fade'}">
<div class="uk-slider-container">
<div class="uk-slideset uk-grid">
<div class="g-news-pro-page">
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-business g-featured-article">
    <div class="g-news-pro-image image-link" style="background-image: url(images/business9.jpg); width: 120px; height: 75px;">
    <a href="##"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="##">Vestibulum faucibus mollis tel massa id est dolor</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>September 15</span>
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Business                                   </span>
    </div>
    </div>
    </div>
    </div>
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-lifestyle g-featured-article">
    <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle12.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Proin volutpat tristique diam quis euismod sed...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>September 14</span>
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
    </div>
    </div>
    </div>
    </div>
</div>
<div class="g-news-pro-page">
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-lifestyle">
    <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle11.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Class aptent taciti socio ad litora est fringi...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>September 13</span>
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
    </div>
    </div>
    </div>
    </div>
    <div class="uk-grid uk-grid-width-1-1">
    <div class="g-news-pro-item horizontal g-cat-fashion">
    <div class="g-news-pro-image image-link" style="background-image: url(images/fashion9.jpg); width: 120px; height: 75px;">
    <a href="#"></a>
    </div>
    <div class="g-info-container">
    <h4 class="g-news-pro-title"><a href="#">Etiam eu sapien at purus ultricies tempor nabh...</a></h4>
    <div class="g-article-details details-show">
    <span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
    <span class="g-article-category">
    <i class="fa fa-folder-open"></i>Fashion                                    </span>
    </div>
    </div>
    </div>
    </div>
</div>
<div class="g-news-pro-page">
    <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-lifestyle">
        <div class="g-news-pro-image image-link" style="background-image: url(images/lifestyle10.jpg); width: 120px; height: 75px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Uisque ac gravida ligula nunc nisi risus ipsum...</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Lifestyle                                  </span>
        </div>
        </div>
        </div>
    </div>
    <div class="uk-grid uk-grid-width-1-1">
        <div class="g-news-pro-item horizontal g-cat-business">
        <div class="g-news-pro-image image-link" style="background-image: url(images/business8.jpg); width: 120px; height: 75px;">
        <a href="#"></a>
        </div>
        <div class="g-info-container">
        <h4 class="g-news-pro-title"><a href="#">Maecenas facilisis dapibus lectus in metus odio</a></h4>
        <div class="g-article-details details-show">
        <span class="g-article-date"><i class="fa fa-clock-o"></i>September 12</span>
        <span class="g-article-category">
        <i class="fa fa-folder-open"></i>Business                                   </span>
        </div>
        </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="g-news-pro-nav">
<div class="g-news-pro-arrows">
<a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
<a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
</div>
</div>
</div>
</div>
</div>  </div>
</div>
</div>
<div         
         class="g-block size-33">
<div class="g-content">
<div class="platform-content"><div class="moduletable ">
<h3 class="g-title"><span>Newsletter</span></h3>
<div class="acymailing_module" id="acymailing_module_formAcymailing73431">
<div class="acymailing_fulldiv" id="acymailing_fulldiv_formAcymailing73431"  >
<form id="formAcymailing73431" action="###" onsubmit="return submitacymailingform('optin','formAcymailing73431')" method="post" name="formAcymailing73431"  >
    <div class="acymailing_module_form" >
    <div class="acymailing_introtext">Stay tuned with our latest news!</div>    <table class="acymailing_form">
    <tr>
    <td class="acyfield_name acy_requiredField">
    <input id="user_name_formAcymailing73431"  onfocus="if(this.value == 'Name') this.value = '';" onblur="if(this.value=='') this.value='Name';" class="inputbox" type="text" name="user[name]" style="width:100%" value="Name" title="Name"/>
    </td> </tr><tr> <td class="acyfield_email acy_requiredField">
    <input id="user_email_formAcymailing73431"  onfocus="if(this.value == 'E-mail') this.value = '';" onblur="if(this.value=='') this.value='E-mail';" class="inputbox" type="text" name="user[email]" style="width:100%" value="E-mail" title="E-mail"/>
    </td> </tr><tr>
    <td  class="acysubbuttons">
    <input class="button subbutton btn btn-primary" type="submit" value="Subscribe" name="Submit" onclick="try{ return submitacymailingform('optin','formAcymailing73431'); }catch(err){alert('The form could not be submitted '+err);return false;}"/>
    </td>
    </tr>
    </table>
    <input type="hidden" name="ajax" value="0" />
    <input type="hidden" name="acy_source" value="module_211" />
    <input type="hidden" name="ctrl" value="sub"/>
    <input type="hidden" name="task" value="notask"/>
    <input type="hidden" name="option" value="com_acymailing"/>
    <input type="hidden" name="hiddenlists" value="1"/>
    <input type="hidden" name="acyformname" value="formAcymailing73431" />
    </div>
</form>
</div>
</div>
</div></div>
</div>
</div>
</div>
</div>
</footer>
    <section id="g-copyright">
        <div class="g-container">
        <div          
                 class="g-grid">
        <div         
                 class="g-block size-50">
        <div class="g-content">
        <div class="moduletable ">
        <div class="g-particle">
        <div class="g-branding branding">
            © Designed by <a href="##">me</a>.
        </div>
        </div>  </div>
        </div>
        </div>
        <div         
                 class="g-block size-50">
        <div class="g-content">
        <div class="moduletable ">
        <div class="g-particle">
        <div class="g-social" >
        <a target="_blank" href="http://facebook.com" data-uk-tooltip title="Facebook"   >
        <span class="fa fa-facebook"></span>
        </a>
        <a target="_blank" href="http://twitter.com" data-uk-tooltip title="Twitter"   >
        <span class="fa fa-twitter"></span>
        </a>
        <a target="_blank" href="https://plus.google.com/" data-uk-tooltip title="Google+"   >
        <span class="fa fa-google-plus"></span>
        </a>
        <a target="_blank" href="http://linkedin.com" data-uk-tooltip title="Linkedin"   >
        <span class="fa fa-linkedin"></span>
        </a>
        <a target="_blank" href="http://dribbble.com" data-uk-tooltip title="Dribbble"   >
        <span class="fa fa-dribbble"></span>
        </a>
        </div>
        </div>  </div>
        </div>
        </div>
        </div>
        </div>
    </section>
</section>
<section id="g-to-top">
<div class="g-container">
<div          
         class="g-grid">
<div         
         class="g-block size-100">
<div class="g-content g-particle">
<div class=" g-particle">
<div class="g-totop style1">
<a href="#" id="g-totop-button" rel="nofollow" data-uk-smooth-scroll>
<i class="fa fa-angle-up"></i> </a>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</div>
<script type="application/javascript" src="js/library.js"></script>
</body>
</html>