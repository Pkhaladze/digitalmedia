@extends('layouts.site_master')
@section('content')
<div id="g-page-surround">
    <div class="g-offcanvas-hide g-offcanvas-toggle" data-offcanvas-toggle><i class="fa fa-fw fa-bars"></i></div>
    <section id="g-container-site" class="g-wrapper">
        @include('includes.header')
        @include('includes.menu')
        @include('includes.main_slider')
        @include('includes.body')



@endsection
